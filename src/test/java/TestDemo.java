import com.md.entity.Users;
import com.md.utils.ClassUtils;
import com.md.utils.MD5Utils;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author 刘骄阳
 * 2021-09-17 23:32
 */
public class TestDemo {
    public static void main(String[] args) {
        //D6B117537C78E1A681586DC416CAE809
        System.out.println(MD5Utils.MD5EncodeUtf8("侬好啊"));
        System.out.println("111".matches("[-]?[\\d]+"));
        Users users = new Users();
        users.setRName("卢本伟");
        System.out.println(ClassUtils.invokedMethod(Users.class, "setRName", new LinkedHashMap<Object, Class<?>>() {{
            this.put("张三", String.class);
        }}, users));
        System.out.println(ClassUtils.invokedMethod(Users.class, "getRName", users));
//        FileUtils.ExcelToPDF();

    }
}

