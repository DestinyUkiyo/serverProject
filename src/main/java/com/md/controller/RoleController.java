package com.md.controller;

import com.md.entity.Role;
import com.md.entity.system.CommonResult;
import com.md.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-09-17 16:18
 */
@RestController
@RequestMapping(value = "/api", name = "角色")
@Api(tags = "角色控制器")
public class RoleController {
    @Resource
    RoleService roleService;

    /**
     * 添加角色
     *
     * @param role 角色对象
     * @return 操作状态
     */
    @PostMapping(value = "/role/insert", name = "新增角色接口")
    @ApiOperation("新增角色接口")
    public CommonResult insert(@RequestBody Role role) {
        return roleService.insert(role);
    }

    /**
     * 修改角色
     * 修改角色名
     *
     * @param role 角色对象
     * @return 操作状态
     */
    @PutMapping(value = "/role/update", name = "修改角色接口")
    @ApiOperation("修改角色接口")
    public CommonResult update(@RequestBody Role role) {
        return roleService.update(role);
    }

    /**
     * 查询角色
     *
     * @return 角色列表
     */
    @GetMapping(value = "/role/select", name = "获取角色列表接口")
    @ApiOperation("获取角色列表接口")
    public CommonResult select() {
        return roleService.select();
    }

    /**
     * 删除角色
     *
     * @param id 要删除的角色id
     * @return 操作状态
     */
    @DeleteMapping(value = "/role/delete", name = "删除角色接口")
    @ApiOperation("删除角色接口")
    public CommonResult delete(@RequestParam Long id) {
        return roleService.delete(id);
    }

}
