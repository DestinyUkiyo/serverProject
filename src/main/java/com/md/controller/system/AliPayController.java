package com.md.controller.system;

import com.md.entity.system.CommonResult;
import com.md.service.AliPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 支付宝功能管理控制器
 *
 * @author 刘骄阳
 * 2021-10-27 15:12
 */
@RestController
@RequestMapping(value = "/system", name = "支付宝")
@Api(value = "支付宝功能管理控制器")
public class AliPayController {
    @Resource
    private AliPayService aliPayService;

    /**
     * 支付宝pc网页支付
     * //获取请求消息体--请求参数
     * <p>
     * //1.获取字符流
     * BufferedReader br = request.getReader();
     * //2.读取数据
     * String line;
     * while ((line = br.readLine()) != null) {
     * System.out.println(line);
     * }
     *
     * @return 操作结果
     */
    @PostMapping(value = "/ali/pcHtmlPay", name = "支付宝电脑网页支付")
    @ApiOperation(value = "支付宝电脑网页支付")
    public CommonResult pcHtmlPay() {
//        return aliPayService.pcHtmlPay();
        return null;
    }

    /**
     *
     */
    @PostMapping(value = "/ali/mobileHtmlPay", name = "支付宝手机网页支付")
    @ApiOperation(value = "支付宝手机网页支付")
    public CommonResult mobileHtmlPay() {
        return aliPayService.mobileHtmlPay();
    }


}

