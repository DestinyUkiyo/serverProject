package com.md.controller.system;

import com.md.entity.system.CommonResult;
import com.md.service.SystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 系统控制器
 *
 * @author 刘骄阳
 * 2021-09-19 13:54
 */
@RestController
@RequestMapping(value = "/system", name = "系统")
@Api(tags = "系统控制器")
public class SystemController {
    @Resource
    SystemService systemService;

    /**
     * 获取全部api接口
     *
     * @return api接口列表
     */
    @GetMapping(value = "/system/getAllRequest", name = "获取全部api接口")
    @ApiOperation("获取全部api接口")
    public CommonResult getAllRequest() {
        return systemService.getAllRequest();
    }

    public CommonResult test() {
        try {
            String a = "D:\\a.txt";
            File file = new File(a);
            String s = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
            //读取有问题只能用UTF-8读取
            String succeedStr = new String(s.getBytes(), StandardCharsets.UTF_8);
            //网络UTF-8查看有问题,只能转成GBK
            FileUtils.writeStringToFile(file, succeedStr, "GBK");
            System.out.println(succeedStr);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return systemService.getAllRequest();
    }


}
