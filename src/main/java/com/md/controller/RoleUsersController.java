package com.md.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.md.entity.RoleUsers;
import com.md.entity.system.CommonResult;
import com.md.entity.system.Page;
import com.md.service.RoleUsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-09-17 18:37
 */
@RestController
@RequestMapping(value = "/api", name = "角色与用户")
@Api(tags = "角色与用户关联关系控制器")
public class RoleUsersController {
    @Resource
    RoleUsersService roleUsersService;

    /**
     * 关联用户与角色
     * 不允许操作
     *
     * @param roleUsers 对象
     * @return 操作结果
     */
    @PostMapping(value = "/roleUsers/insert", name = "关联角色与用户接口")
    @ApiOperation("关联角色与用户接口")
    public CommonResult insert(@RequestBody RoleUsers roleUsers) {
        return roleUsersService.insert(roleUsers);
    }

    /**
     * 修改用户与角色
     *
     * @param roleUsers 对象
     * @return 操作结果
     */
    @PutMapping(value = "/roleUsers/update", name = "修改角色与用户关系接口")
    @ApiOperation("修改角色与用户关系接口")
    public CommonResult update(@RequestBody RoleUsers roleUsers) {
        return roleUsersService.update(roleUsers);
    }

    /**
     * 用户与角色关联列表
     *
     * @param page 分页
     * @return 用户与角色关联列表
     */
    @PostMapping(value = "/roleUsers/select", name = "获取角色与用户关联列表")
    @ApiOperation("获取角色与用户关联列表")
    public CommonResult select(@RequestBody Page page) {
        return roleUsersService.select(page);
    }

    /**
     * 删除用户与角色关联关系
     *
     * @param id id
     * @return 操作结果
     */
    @DeleteMapping(value = "/roleUsers/delete", name = "删除角色与用户关联接口")
    @ApiOperation("删除角色与用户关联接口")
    public CommonResult delete(@RequestParam Long id) {
        return roleUsersService.delete(id);
    }


}
