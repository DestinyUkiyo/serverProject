package com.md.controller;

import com.md.entity.Users;
import com.md.entity.system.CommonResult;
import com.md.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-10-08 0:14
 */
@RestController
@RequestMapping(value = "/api", name = "登录")
@Api(tags = "登录控制器")
public class LoginController {
    @Resource
    LoginService loginService;

    /**
     * 账号登录
     *
     * @param users users对象
     * @return 操作结果
     */
    @PostMapping(value = "/login/accountLogin", name = "账号登录")
    @ApiOperation("账号登录")
    public CommonResult login(@RequestBody Users users) {
        return loginService.login(users);
    }
}
