package com.md.controller;

import com.md.entity.RoleApi;
import com.md.entity.system.CommonResult;
import com.md.service.RoleApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-09-17 22:50
 */
@RestController
@RequestMapping(value = "/api", name = "角色与api")
@Api(tags = "角色与api接口关联关系控制器")
public class RoleApiController {
    @Resource
    RoleApiService roleApiService;

    /**
     * 关联角色和api
     * 已弃用:创建角色时,会自动关联一个默认的登录权限
     *
     * @param roleApi 对象
     * @return 操作结果
     */
    @PostMapping(value = "/roleApi/insert", name = "关联角色和api")
    @ApiOperation("关联角色与api接口")
    public CommonResult insert(@RequestBody RoleApi roleApi) {
        return roleApiService.insert(roleApi);
    }

    /**
     * 关联角色和api
     *
     * @param roleApi 对象
     * @return 操作结果
     */
    @PutMapping(value = "/roleApi/update", name = "修改角色与api")
    @ApiOperation("修改角色与api接口")
    public CommonResult update(@RequestBody RoleApi roleApi) {
        return roleApiService.update(roleApi);
    }

    /**
     * 通过id查询角色拥有的权限
     *
     * @param id 角色id
     * @return 操作结果
     */
    @GetMapping(value = "/roleApi/select", name = "获取角色与api关联列表接口")
    @ApiOperation("获取角色与api关联列表接口")
    public CommonResult select(@RequestParam Long id) {
        return roleApiService.select(id);
    }

    /**
     * 删除角色与api的关联关系
     * 不允许操作
     *
     * @param id 要删除的id
     * @return 操作结果
     */
    @DeleteMapping(value = "/roleApi/delete", name = "删除角色与api关联关系接口")
    @ApiOperation("删除角色与api关联关系接口")
    public CommonResult delete(@RequestParam Long id) {
        return roleApiService.delete(id);
    }
}
