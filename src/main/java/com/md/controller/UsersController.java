package com.md.controller;

import com.md.entity.Users;
import com.md.entity.system.CommonResult;
import com.md.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-09-16 22:17
 */
@RestController
@RequestMapping(value = "/api", name = "用户")
@Api(tags = "用户控制器")
public class UsersController {
    @Resource
    UsersService usersService;

    /**
     * 添加用户
     *
     * @param users 用户对象
     * @return 操作结果
     */
    @PostMapping(value = "/users/insert", name = "新增用户接口")
    @ApiOperation("新增用户接口")
    public CommonResult insert(@RequestBody Users users) {
        return usersService.insert(users);
    }

    /**
     * 注册用户
     *
     * @param users 用户对象
     * @return 操作结果
     */
    @PostMapping(value = "/users/register", name = "注册用户")
    public CommonResult registerUsers(@RequestBody Users users) {
        return usersService.registerUsers(users);
    }

    /**
     * 修改用户
     *
     * @param users 用户对象
     * @return 操作结果
     */
    @PutMapping(value = "/users/update", name = "修改用户接口")
    @ApiOperation("修改用户接口")
    public CommonResult update(@RequestBody Users users) {
        return null;
    }

    /**
     * 获取用户列表接口
     *
     * @return 用户列表
     */
    @GetMapping(value = "/users/select", name = "获取用户列表接口")
    @ApiOperation("获取用户列表接口")
    public CommonResult select() {
        return null;
    }

    /**
     * 冻结用户
     *
     * @param id 用户唯一标识id
     * @return 操作结果
     */
    @DeleteMapping(value = "/users/delete", name = "删除用户接口")
    @ApiOperation("删除用户接口")
    public CommonResult delete(@RequestParam Long id) {
        return null;
    }
}
