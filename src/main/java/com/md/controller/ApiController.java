package com.md.controller;

import com.md.entity.Api;
import com.md.entity.system.CommonResult;
import com.md.service.ApiService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-09-17 19:02
 */
@RestController
@RequestMapping(value = "/api", name = "api")
@io.swagger.annotations.Api(tags = "Api接口控制器")
public class ApiController {

    @Resource
    ApiService apiService;

    /**
     * 添加api接口
     * 不允许操作
     *
     * @param api 对象
     * @return 操作结果
     */
    @PostMapping(value = "/api/insert", name = "添加api接口")
    @ApiOperation("新增api接口")
    public CommonResult insert(@RequestBody Api api) {
        return apiService.insert(api);
    }

    /**
     * 修改api接口
     * 修改功能名
     *
     * @param api 对象
     * @return 操作结果
     */
    @PutMapping(value = "/api/update", name = "修改api接口")
    @ApiOperation("修改api接口")
    public CommonResult update(@RequestBody Api api) {
        return apiService.update(api);
    }

    /**
     * 获取api列表
     *
     * @return api列表
     */
    @GetMapping(value = "/api/select", name = "获取api列表接口")
    @ApiOperation("获取api列表接口")
    public CommonResult select() {
        return apiService.select();
    }

    /**
     * 删除api接口
     * 不允许操作
     *
     * @param id id
     * @return 操作结果
     */
    @DeleteMapping(value = "/api/delete", name = "删除api接口")
    @ApiOperation("删除api接口")
    public CommonResult delete(@RequestParam Long id) {
        return apiService.delete(id);
    }
}
