package com.md.handler.security;

import com.md.entity.system.CommonResult;
import com.md.entity.system.Dictionaries;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 访问接口没有权限时,自定义的返回结果
 *
 * @author 刘骄阳
 * 2021-09-17 11:31
 */
@Component
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {

    /**
     * @param request       request
     * @param response      response
     * @param authException authException
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(Dictionaries.GSON.toJson(CommonResult.permission()));
        response.getWriter().flush();
    }
}
