package com.md.handler.security;

import com.md.entity.system.CommonResult;
import com.md.entity.system.Dictionaries;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 配置 未登录,或token失效 自定义的返回结果
 *
 * @author 刘骄阳
 * 2021-09-17 11:31
 */
@Component
public class MyAccessDeniedHandler implements AccessDeniedHandler {
    /**
     * 未登录,或token失效,访问接口时调用
     *
     * @param request               request
     * @param response              response
     * @param accessDeniedException accessDeniedException
     * @throws IOException      IOException
     * @throws ServletException ServletException
     */
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(Dictionaries.GSON.toJson(CommonResult.unauthorized()));
        response.getWriter().flush();
    }
}
