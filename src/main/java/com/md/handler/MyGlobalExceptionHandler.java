package com.md.handler;

import com.md.entity.system.CommonResult;
import com.md.entity.system.Dictionaries;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 全局异常处理
 * <p>
 * 使用@ControllerAdvice来声明一些全局性的东西
 * -@ExceptionHandler注解标注的方法：用于捕获Controller中抛出的不同类型的异常，从而达到异常全局处理的目的；
 * -@InitBinder注解标注的方法：用于请求中注册自定义参数的解析，从而达到自定义请求参数格式的目的；
 * -@ModelAttribute注解标注的方法：表示此方法会在执行目标Controller方法之前执行 。
 *
 * @author 刘骄阳
 * 2021-09-24 0:14
 */
@RestControllerAdvice
public class MyGlobalExceptionHandler {

    /**
     * 处理异常: 对报的错误进行操作,返回给页面为json格式
     * 成功,但是显示数据不正确,需要修改,http://localhost:9527/system/getAllRequest
     * {
     * "code": 500,
     * "message": "-1",
     * "data": ""
     * }
     */
//    @ExceptionHandler
    public void operationException(HttpServletResponse response, Exception exception) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().println(Dictionaries.GSON.toJson(CommonResult.result(500, "出现异常", exception)));
        response.getWriter().flush();
    }
}
