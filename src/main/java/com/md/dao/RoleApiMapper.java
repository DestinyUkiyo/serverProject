package com.md.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.md.entity.RoleApi;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author 刘骄阳
 * 2021-09-17 23:02
 */
@Repository
@Mapper
public interface RoleApiMapper extends BaseMapper<RoleApi> {
}
