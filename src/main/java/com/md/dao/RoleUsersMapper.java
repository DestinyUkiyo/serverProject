package com.md.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.md.entity.RoleUsers;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author 刘骄阳
 * 2021-09-17 18:39
 */
@Repository
@Mapper
public interface RoleUsersMapper extends BaseMapper<RoleUsers> {
}
