package com.md.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.md.entity.Api;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author 刘骄阳
 * 2021-09-17 19:07
 */
@Repository
@Mapper
public interface ApiMapper extends BaseMapper<Api> {
}
