package com.md.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.md.entity.Users;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author 刘骄阳
 * 2021-09-16 22:20
 */
@Repository
@Mapper
public interface UsersMapper extends BaseMapper<Users> {
}
