package com.md.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.md.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author 刘骄阳
 * 2021-09-17 16:48
 */
@Repository
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
}
