package com.md;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author 刘骄阳
 * 2021-09-16 21:18
 */
@MapperScan("com.md.dao")
@SpringBootApplication
@ServletComponentScan
public class ApplicationStarter {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ApplicationStarter.class, args);
        System.err.println("--------------------------------------");
        System.err.println("\t\t\t\t启动成功");
        System.err.println("--------------------------------------");
    }
}
