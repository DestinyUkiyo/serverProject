package com.md.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

/**
 * 用户表
 *
 * @author 刘骄阳
 * 2021-09-16 21:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("users")
@ApiModel("用户表")
public class Users implements UserDetails {
    /**
     * 唯一标识Id
     */
    @TableId("u_id")
    @JsonProperty("uid")
    @ApiModelProperty("唯一标识id")
    private Long uId;
    /**
     * 用户名
     */
    @TableField("u_name")
    @JsonProperty("uName")
    @ApiModelProperty("用户名")
    private String uName;
    /**
     * 密码
     */
    @TableField("u_password")
    @JsonProperty("uPassword")
    @ApiModelProperty("密码")
    private String uPassword;
    /**
     * 账号
     */
    @TableField("u_account")
    @JsonProperty("uAccount")
    @ApiModelProperty("账号")
    private Long uAccount;
    /**
     * 头像
     */
    @TableField("u_head_icon")
    @JsonProperty("uHeadIcon")
    @ApiModelProperty("头像")
    private String uHeadIcon;
    /**
     * 性别
     */
    @TableField("u_sex")
    @JsonProperty("uSex")
    @ApiModelProperty("性别")
    private Boolean uSex;
    /**
     * 手机号
     */
    @TableField("u_phone")
    @JsonProperty("uPhone")
    @ApiModelProperty("手机号")
    private String uPhone;
    /**
     * 身份证
     */
    @TableField("u_identity")
    @JsonProperty("uIdentity")
    @ApiModelProperty("身份证")
    private String uIdentity;
    /**
     * 解冻时间
     */
    @TableField("u_frozen")
    @JsonProperty("uFrozen")
    @ApiModelProperty("解冻时间")
    private Date uFrozen;
    /**
     * 生日
     */
    @TableField("u_birthday")
    @JsonProperty("uBirthday")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("出生日期")
    private Date uBirthday;
    /**
     * 最后登录时间
     */
    @TableField("u_last_login_time")
    @JsonProperty("uLastLoginTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("最后登录时间")
    private Date uLastLoginTime;
    /**
     * 是否可用
     */
    @TableField("u_enable")
    @JsonProperty("uEnable")
    @ApiModelProperty("是否可用(是否冻结)")
    private Boolean uEnable;
    /**
     * 创建时间
     */
    @TableField("u_create_date")
    @JsonProperty("uCreateDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("创建时间")
    private Date uCreateDate;
    /**
     * 修改时间
     */
    @TableField("u_modify_date")
    @JsonProperty("uModifyDate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("修改时间")
    private Date uModifyDate;


    /**
     * 允许访问的api接口
     */
    @TableField(exist = false)
    @ApiModelProperty("允许访问的api接口")
    private ArrayList<Api> authorities;

    /**
     * 用户所属角色Id
     */
    @TableField(exist = false)
    @ApiModelProperty("用户所属角色Id")
    private Long rId;

    /**
     * 用户所属角色名
     */
    @TableField(exist = false)
    @ApiModelProperty("用户所属角色名")
    private String rName;

    /**
     * token
     */
    @TableField(exist = false)
    @ApiModelProperty("token")
    private String token;


    /**
     * 返回当前用户允许调用的api接口
     */
    @Override
    public ArrayList<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    /**
     * 返回当前用户的密码
     */
    @Override
    public String getPassword() {
        return this.uPassword;
    }

    /**
     * 返回当前用户的账号
     */
    @Override
    public String getUsername() {
        return this.uAccount.toString();
    }

    /**
     * 指示用户的帐户是否已过期。过期的帐户无法进行身份验证
     * true: 未过期
     * false: 已过期
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 返回用户状态;已被锁定或未锁定。被锁定的用户无法通过认证。
     */
    @Override
    public boolean isAccountNonLocked() {
        return this.uEnable;
    }

    /**
     * 指示用户的凭据(密码)是否已过期。过期的凭证将阻止身份验证。
     * true: 未过期
     * false: 已过期
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 返回用户状态;启用还是禁用。禁用的用户不能通过认证。
     */
    @Override
    public boolean isEnabled() {
        return this.uEnable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Users)) {
            return false;
        }
        Users users = (Users) o;
        return uId.equals(users.uId) &&
                uAccount.equals(users.uAccount) &&
                uCreateDate.equals(users.uCreateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uId, uAccount, uCreateDate);
    }

    /**
     * 从集合中删除对象
     *
     * @param onlyUser 集合
     */
    public HashMap<String, Users> removeMap(HashMap<String, Users> onlyUser) {
        if (onlyUser != null) {
            for (String token : onlyUser.keySet()) {
                //检索出相匹配的Users
                if (onlyUser.get(token).equals(this)) {
                    //删除
                    onlyUser.remove(token);
                    //停止循环
                    break;
                }
            }
        }
        return onlyUser;
    }
}
