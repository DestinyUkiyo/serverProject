package com.md.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色表
 *
 * @author 刘骄阳
 * 2021-09-17 16:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("role")
@ApiModel("角色表")
public class Role {
    /**
     * 唯一标识id
     */
    @TableId("r_id")
    @JsonProperty("rid")
    @ApiModelProperty("唯一标识id")
    private Long rId;

    /**
     * 角色名
     */
    @TableField("r_name")
    @JsonProperty("rName")
    @ApiModelProperty("角色名")
    private String rName;

    /**
     * apiId,用逗号隔开
     */
    @TableField(exist = false)
    @JsonProperty("apiIds")
    @ApiModelProperty("apiId,用逗号隔开")
    private String apiIds;

}
