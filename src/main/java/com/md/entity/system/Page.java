package com.md.entity.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 刘骄阳
 * 2021-10-20 22:20
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("分页")
public class Page {
    /**
     * 行数
     */
    @ApiModelProperty("行数")
    private Integer pageSize;
    /**
     * 页码
     */
    @ApiModelProperty("页码")
    private Integer pageNum;
    /**
     * 总页数
     */
    @ApiModelProperty("总页数")
    private Integer total;

    public void setPageSize(Integer pageSize) {
        if (pageSize == null || pageSize <= 0) {
            this.pageSize = 10;
        } else {
            this.pageSize = pageSize;
        }
    }

    public void setPageNum(Integer pageNum) {
        if (pageNum == null || pageNum <= 0) {
            this.pageNum = 1;
        } else {
            this.pageNum = pageNum;
        }
    }
}
