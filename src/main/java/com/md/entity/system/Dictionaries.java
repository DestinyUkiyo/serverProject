package com.md.entity.system;

import com.google.gson.Gson;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.*;

/**
 * @author 刘骄阳
 * 2021-09-17 11:40
 */
@ApiModel("字典类")
public class Dictionaries {
    // 工具类
    /**
     * 工具类-地球半径
     */
    @ApiModelProperty("计算工具类--地球半径")
    public static final Double EARTH_RADIUS = 6378.137;
    //校验工具类
    /**
     * 是否是纯数字
     */
    @ApiModelProperty("校验工具类--匹配整数")
    public static final String IS_NUMBER = "[-]?[\\d]+";

    // 状态码
    /**
     * 状态码-成功
     */
    @ApiModelProperty("状态码--成功")
    public static final Integer SUCCESS_CODE = 200;
    /**
     * 状态码-失败
     */
    @ApiModelProperty("状态码--失败")
    public static final Integer ERROR_CODE = 500;
    /**
     * 状态码-无权限
     */
    @ApiModelProperty("状态码--没有权限")
    public static final Integer PERMISSION_CODE = 403;
    /**
     * 状态码-未登录
     */
    @ApiModelProperty("状态码--没有登录")
    public static final Integer UNAUTHORIZED_CODE = 401;
    /**
     * 状态码-查无资源
     */
    @ApiModelProperty("状态码--没有资源")
    public static final Integer UN_EXISTENT_CODE = 404;

    // 实体类
    /**
     * 实体类-Json化实体类
     */
    @ApiModelProperty("实体类--Gson")
    public static final Gson GSON = new Gson();

    //字典
    /**
     * 用于分割的
     */
    @ApiModelProperty("用于分割的")
    public static final String SPLIT = ",";

    /**
     * 未定义
     */
    @ApiModelProperty("未定义")
    public static final String UNKNOWN = "unknown";

    /**
     * 获取ip的途径
     */
    @ApiModelProperty("获取ip的途径")
    public static final String[] ADDRESS_NAME = {"X-forwarded-for", "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR"};

    /**
     * 用于获取被指定注解标注的方法
     */
    @ApiModelProperty("可能的请求方式")
    public static final ArrayList<Class> REQUEST_CLASS = new ArrayList<>(Arrays.asList(PostMapping.class, PutMapping.class, GetMapping.class, DeleteMapping.class));

    /**
     * 服务器操作系统
     */
    @ApiModelProperty("服务器操作系统")
    public static final String OS_NAME = System.getProperty("os.name");


    /**
     * 没有权限返回的数据
     */
    @ApiModelProperty("没有权限返回的数据")
    public static final ArrayList<ConfigAttribute> UN_PERMISSION = new ArrayList<ConfigAttribute>() {
        {
            this.add(new SecurityConfig("403"));
        }
    };

}
