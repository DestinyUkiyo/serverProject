package com.md.entity.system;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 专门返回给前端的实体类
 *
 * @author 刘骄阳
 * 2021-09-16 22:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("专门返回给前端的实体类")
public class CommonResult {
    /**
     * 状态码
     */
    @ApiModelProperty("状态码")
    private Integer code;

    /**
     * 返回消息
     */
    @ApiModelProperty("返回消息")
    private String message;

    /**
     * 返回数据
     */
    @ApiModelProperty("返回数据")
    private Object data;

    /**
     * 自定义返回
     *
     * @param code    状态码
     * @param message 返回消息
     * @param data    返回数据
     * @return CommentResult对象
     */
    public static CommonResult result(Integer code, String message, Object data) {
        return new CommonResult(code, message, data);
    }

    /**
     * 操作成功[200]
     *
     * @param data 返回数据
     * @return CommentResult对象
     */
    public static CommonResult success(Object data) {
        return new CommonResult(Dictionaries.SUCCESS_CODE, "操作成功!", data);
    }

    /**
     * 操作失败[500]
     *
     * @param data 返回数据
     * @return CommentResult对象
     */
    public static CommonResult error(Object data) {
        return new CommonResult(Dictionaries.ERROR_CODE, "操作失败!", data);
    }

    /**
     * 没有权限[403]
     *
     * @return CommentResult对象
     */
    public static CommonResult permission() {
        return new CommonResult(Dictionaries.PERMISSION_CODE, "权限不足!", "/login");
    }

    /**
     * 登录失效或已过期[401]
     *
     * @return CommentResult对象
     */
    public static CommonResult unauthorized() {
        return new CommonResult(Dictionaries.UNAUTHORIZED_CODE, "暂未登录或token已经过期!", "/login");
    }

    /**
     * 找不到资源[404]
     *
     * @return CommentResult对象
     */
    public static CommonResult unExistent() {
        return new CommonResult(Dictionaries.UN_EXISTENT_CODE, "找不到页面", "/404");
    }

}
