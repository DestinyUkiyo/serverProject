package com.md.entity.system;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 刘骄阳
 * 2021-10-03 14:24
 */
@Data
public class MyConfig {

    /**
     * Windows系统下的文件上传路径
     */
    @ApiModelProperty("Windows系统下的文件上传路径")
    public String windowsFileUrl;

    /**
     * Linux系统下的文件上传路径
     */
    @ApiModelProperty("Linux系统下的文件上传路径")
    public String linuxFileUrl;


    /**
     * 拦截配置-文件访问地址
     */
    @ApiModelProperty("拦截配置-文件访问地址")
    public String loadFileUrl;

    /**
     * 文件访问地址
     */
    @ApiModelProperty("文件访问地址")
    public String accessAddress;

}
