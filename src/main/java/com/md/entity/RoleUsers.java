package com.md.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色与用户的中间表
 *
 * @author 刘骄阳
 * 2021-09-17 18:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("role_users")
@ApiModel("角色与用户中间表")
public class RoleUsers {
    /**
     * 唯一标识id
     */
    @TableId("ur_id")
    @JsonProperty("ruId")
    @ApiModelProperty("唯一标识id")
    private Long urId;

    /**
     * 角色id
     */
    @TableField("r_id")
    @JsonProperty("rid")
    @ApiModelProperty("角色id")
    private Long rId;

    /**
     * 用户id
     */
    @TableField("u_id")
    @JsonProperty("uid")
    @ApiModelProperty("用户id")
    private Long uId;

}
