package com.md.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;

import java.util.Objects;

/**
 * API表
 *
 * @author 刘骄阳
 * 2021-09-17 18:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("api")
@ApiModel("api接口实体类")
public class Api implements GrantedAuthority {

    /**
     * 唯一标识id
     * //    @NotNull
     */
    @TableId("api_id")
    @JsonProperty("apiId")
    @ApiModelProperty("唯一标识id")
    private Long apiId;

    /**
     * api名字
     */
    @TableField("api_name")
    @JsonProperty("apiName")
    @ApiModelProperty("api名字")
    private String apiName;

    /**
     * api路径
     */
    @TableField("api_url")
    @JsonProperty("apiUrl")
    @ApiModelProperty("api路径")
    private String apiUrl;


    /**
     * 格式化json字符串为Api对象
     *
     * @param json   json字符串
     * @param suffix 前缀
     * @return MyRequestMethod
     */
    public static Api init(String json, String suffix) {
        Api temp = new Api();
        json = json.substring(json.indexOf("name") + 5);
        //因为东西都是固定的,所以获取也是固定的
        temp.setApiName(json.substring(0, json.indexOf("produces") - 2));
        json = json.substring(json.indexOf("value") + 7);
        temp.setApiUrl(suffix + json.substring(0, json.indexOf("consumes") - 3));
        return temp;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Api)) {
            return false;
        }
        Api api = (Api) o;
        return apiUrl.equals(api.apiUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(apiUrl);
    }


    /**
     * 返回apiId
     */
    @Override
    public String getAuthority() {
        if (this.apiId != null) {
            return this.apiId.toString();
        }
        return null;
    }

    public Api(Long apiId) {
        this.apiId = apiId;
    }
}
