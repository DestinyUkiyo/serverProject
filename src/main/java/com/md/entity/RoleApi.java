package com.md.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * 角色与api接口中间表
 *
 * @author 刘骄阳
 * 2021-09-17 22:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("role_api")
@ApiModel("角色和Api接口中间表")
public class RoleApi {
    /**
     * 唯一标识id
     */
    @TableId("ra_id")
    @JsonProperty("raId")
    @ApiModelProperty("唯一标识id")
    private Long raId;

    /**
     * 角色id
     */
    @TableField("r_id")
    @JsonProperty("rid")
    @ApiModelProperty("角色id")
    private Long rId;

    /**
     * apiId,通过逗号间隔
     */
    @TableField("api_id")
    @JsonProperty("apiId")
    @ApiModelProperty("api接口Id")
    private String apiId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RoleApi)) {
            return false;
        }
        RoleApi roleApi = (RoleApi) o;
        return Objects.equals(raId, roleApi.raId) &&
                Objects.equals(rId, roleApi.rId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(raId, rId);
    }
}
