package com.md.utils;


import com.aspose.cells.*;
import com.aspose.slides.Presentation;
import com.aspose.words.*;
import com.aspose.words.SaveFormat;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.Charset;

/**
 * 文件工具类
 *
 * @author 刘骄阳
 * 2021-09-24 10:09
 */
public class FilesUtils {
    /**
     * 加载license 用于破解 不生成水印
     *
     * @param index 标识,是word、excel、ppt哪一个转pdf
     * @return 验证结果
     */
    @SneakyThrows
    private static boolean getLicense(Integer index) {
        InputStream is = null;
        try {
            //加载license.xml
            is = FilesUtils.class.getClassLoader().getResourceAsStream("License.xml");
            switch (index) {
                case 0:
                    //word转pdf的License
                    com.aspose.words.License wordLicense = new com.aspose.words.License();
                    wordLicense.setLicense(is);
                    break;
                case 1:
                    //excel转pdf的License
                    com.aspose.cells.License excelLicense = new com.aspose.cells.License();
                    excelLicense.setLicense(is);
                    break;
                case 2:
                    //ppt转pdf的License
                    com.aspose.slides.License pptLicense = new com.aspose.slides.License();
                    pptLicense.setLicense(is);
                    break;
                default:
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return false;
    }

    /**
     * word转pdf
     *
     * @param wordPath 需要被转换的excel全路径带文件名
     * @param pdfPath  转换之后pdf的全路径带文件名
     */
    @SneakyThrows
    public static void wordToPdf(String wordPath, String pdfPath) {
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (getLicense(0)) {
            return;
        }
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(new File(pdfPath));
            Document doc = new Document(wordPath);
            doc.save(fileOutputStream, SaveFormat.PDF);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * excel转pdf
     *
     * @param excelPath 需要被转换的excel全路径带文件名
     * @param pdfPath   转换之后pdf的全路径带文件名
     */
    public static void excelToPdf(String excelPath, String pdfPath) {
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (getLicense(1)) {
            return;
        }
        FileOutputStream fileOS = null;
        try {
            Workbook wb = new Workbook(excelPath);
            fileOS = new FileOutputStream(new File(pdfPath));
            wb.save(fileOS, com.aspose.cells.SaveFormat.PDF);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOS != null) {
                try {
                    fileOS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ppt转pdf
     *
     * @param pptPath 需要被转换的ppt全路径带文件名
     * @param pdfPath 转换之后pdf的全路径带文件名
     */
    public static void pptToPdf(String pptPath, String pdfPath) {
        // 验证License 若不验证则转化出的pdf文档会有水印产生
        if (getLicense(2)) {
            return;
        }
        FileOutputStream fileOS = null;
        try {
            Presentation pres = new Presentation(pptPath);
            fileOS = new FileOutputStream(new File(pdfPath));
            pres.save(fileOS, com.aspose.slides.SaveFormat.Pdf);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOS != null) {
                try {
                    fileOS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
