package com.md.utils;

import com.md.entity.system.Dictionaries;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 刘骄阳
 * 2021-10-09 11:27
 */
public class RequestUtil {
    /**
     * 获取ip地址
     *
     * @param request request
     * @return ip地址
     */
    public static String getIPAddress(HttpServletRequest request) {
        String ip = null;
        String[] temp = Dictionaries.ADDRESS_NAME;
        for (String head : temp) {
            ip = request.getHeader(head);
            if (ip != null && ip.length() != 0 && !Dictionaries.UNKNOWN.equalsIgnoreCase(ip)) {
                //说明获取到ip地址了
                break;
            }
        }
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        return "0:0:0:0:0:0:0:1".equalsIgnoreCase(ip) ? "127.0.0.1" : ip;
    }
}
