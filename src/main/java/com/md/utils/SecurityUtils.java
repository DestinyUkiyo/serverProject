package com.md.utils;

import com.md.entity.Api;
import com.md.entity.RoleApi;
import com.md.entity.system.Dictionaries;
import com.md.entity.system.MyConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 刘骄阳
 * 2021-10-11 13:57
 */
@Component
@RequiredArgsConstructor
public class SecurityUtils {
    private final RedisUtils redis;
    private final MyConfig myConfig;

    /**
     * 不需要权限的集合
     */
    public static final ArrayList<String> FREE_LIST = new ArrayList<>();
    /**
     * 禁止访问的权限集合
     */
    public static final ArrayList<String> PROHIBIT_LIST = new ArrayList<>();

    public void init(List<RoleApi> roleApiList) {
        //从redis中读取 后端使用的api
        HashMap<Long, Api> requestMap = redis.getRequestMap();
        HashMap<String, ArrayList<ConfigAttribute>> securityMap = new HashMap<>(16);


        HashMap<Long, ArrayList<ConfigAttribute>> map = new HashMap<>(roleApiList.size());

        //每一个键值对都对应一个list集合
        for (Long id : requestMap.keySet()) {
            map.put(id, new ArrayList<>());
        }

        //为字典中的角色与api关联关系赋值
        Map<Long, String> roleApiMap = roleApiList.stream().collect(Collectors.toMap(RoleApi::getRId, RoleApi::getApiId));

        //配置每个请求路径所需的角色,或权限
        roleApiList.forEach(roleApi -> {
            //将每一个apiId根据,分割
            String[] temp = roleApi.getApiId().split(",");
            for (String id : temp) {
                map.get(Long.parseLong(id)).add(new SecurityConfig(roleApi.getRId().toString()));
            }
        });
        //根据,id获取api的路径,并将路径所需权限存入map中
        map.forEach((id, list) -> securityMap.put(requestMap.get(id).getApiUrl(), list));
        //初始化不需要权限的地址
        FREE_LIST.add(myConfig.accessAddress);
        FREE_LIST.add("/api/login/");
        FREE_LIST.add("/system/ali/mobileHtmlPay");
        //初始化禁止访问的权限地址
        PROHIBIT_LIST.add("/api/roleApi/insert");
        PROHIBIT_LIST.add("/api/roleApi/delete");
        PROHIBIT_LIST.add("/api/api/insert");
        PROHIBIT_LIST.add("/api/api/delete");
        PROHIBIT_LIST.add("/api/roleUsers/insert");

        //初始化所有角色与api的关联
        redis.set("roleApiMap", roleApiMap);
        //初始化需要权限的map集合
        redis.set("securityMap", securityMap);
    }


    /**
     * 重新加载权限
     *
     * @param rId  角色id
     * @param list apiId集合
     */
    public void reloadRoleApi(final Long rId, ArrayList<String> list) {
        //todo 此处应添加锁
        HashMap<Long, Api> requestMap = redis.getRequestMap();
        HashMap<Long, String> roleApiMap = redis.getRoleApiMap();
        HashMap<String, ArrayList<ConfigAttribute>> securityMap = redis.getSecurityMap();

        //通过rId获取修改之前的apiId
        String apis = roleApiMap.get(rId);
        if (StringUtils.isNotBlank(apis)) {
            ArrayList<String> apiList = new ArrayList<>(Arrays.asList(apis.split(",")));
            //从REQUEST_MAP去除,每一个角色id的数据
            apiList.forEach(apiId -> {
                //根据请求路径获取所需角色
                ArrayList<ConfigAttribute> temp = securityMap.get(requestMap.get(Long.parseLong(apiId)).getApiUrl());
                for (int i = 0; i < temp.size(); i++) {
                    //判断权限是否一致
                    if (temp.get(i).getAttribute().equalsIgnoreCase(rId.toString())) {
                        //一致就删除
                        temp.remove(i);
                        break;
                    }
                }
                //重新赋值
                securityMap.put(requestMap.get(Long.parseLong(apiId)).getApiUrl(), temp);
            });
        }
        //重新添加权限
        list.forEach(apiId -> {
            //根据请求路径获取所需角色
            ArrayList<ConfigAttribute> temp = securityMap.get(requestMap.get(Long.parseLong(apiId)).getApiUrl());
            //将当前角色追加进集合中
            temp.add(new SecurityConfig(rId.toString()));
            //重新赋值
            securityMap.put(requestMap.get(Long.parseLong(apiId)).getApiUrl(), temp);
        });
        redis.set("securityMap", securityMap);
    }

    /**
     * 校验security权限
     *
     * @param requestUrl 请求地址
     * @return 是否拥有此权限, 无需权限,返回null
     */
    public ArrayList<ConfigAttribute> authorityCheck(String requestUrl) {
        //判断是否为空
        if (StringUtils.isNotBlank(requestUrl)) {
            for (String freeUrl : FREE_LIST) {
                if (requestUrl.indexOf(freeUrl) == 0) {
                    //说明访问的是不需要权限的地址,准许通过
                    return null;
                }
            }
            for (String prohibitUrl : PROHIBIT_LIST) {
                if (requestUrl.indexOf(prohibitUrl) == 0) {
                    //说明访问的是禁止访问的地址
                    return Dictionaries.UN_PERMISSION;
                }
            }

            ArrayList<ConfigAttribute> list = redis.getSecurityMap().get(requestUrl);
            if (list != null && list.size() > 0) {
                list.forEach(System.err::println);
                //有这个请求,并且也有角色拥有这个权限
                return list;
            }
            //说明这个请求不存在,或者 没有用户拥有这个权限
        }
        return Dictionaries.UN_PERMISSION;
    }

}
