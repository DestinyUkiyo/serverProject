package com.md.utils;


import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Objects;

/**
 * MD5加密
 *
 * @author 刘骄阳
 */
public class MD5Utils {
    /**
     * 将字节数组处理成字符串
     *
     * @param b 字节数组
     * @return 字符串
     */
    private static String byteArrayToHexString(byte[] b) {
        StringBuilder resultSb = new StringBuilder();
        for (int i = 0; i < b.length; i++) {
            resultSb.append(byteToHexString(b[i]));
        }

        return resultSb.toString();
    }

    /**
     * 将字节加密
     *
     * @param b 字节
     * @return 解密后的数值
     */
    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            //如果字节小于0,使其增加256
            n += 256;
        }
        //除以16
        int d1 = n / 16;
        //取余16
        int d2 = n % 16;
        //将得出来的值,对应的16进制的字符相加
        return hexDigits[d1] + hexDigits[d2];
    }

    /**
     * 进行MD5编码
     *
     * @param origin 加密前的文本
     * @return 加密后转为大写的文本
     */
    private static String MD5Encode(final Object origin) {
        String resultString = null;
        try {
            resultString = origin + "";
            MessageDigest md = MessageDigest.getInstance("MD5");
            //使用MD5加密,要加密的文本的UTF-8编码的字节,并进行下一步操作
            resultString = byteArrayToHexString(md.digest(resultString.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception exception) {
        }
        return resultString.toUpperCase();
    }

    /**
     * 加密字符串
     *
     * @param origin 加密前的文本
     * @return 加密后的文本
     */
    public static String MD5EncodeUtf8(Object origin) {
        return MD5Encode(origin);
    }


    /**
     * 16进制对应的字符
     */
    private static final String[] hexDigits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

}
