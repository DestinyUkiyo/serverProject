package com.md.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author 刘骄阳
 * 2021-11-01 20:21
 */
public class DateUtils {
    static String[] dateStr = {"yyyy-MM-dd", "yyyy-MM-d", "yyyy-M-dd", "yyyy-M-d", "yyyyMMdd", "yyyyMMd", "yyyyMdd", "yyyyMd", "yyyy年MM月dd日",
            "yyyy年MM月d日", "yyyy年M月dd日", "yyyy年M月d日", "yyyy/MM/dd", "yyyy/MM/d", "yyyy/M/dd", "yyyy/M/d", "yyyy.MM.dd", "yyyy.MM.d",
            "yyyy.M.dd", "yyyy.M.d", "yyyy年MM月dd号"};

    /**
     * 获取今天最小的时间
     *
     * @return 今天最小的时间
     */
    public static Date getTodayMin() {
        return Date.from(LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MIN).atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 获取今天最大的时间
     *
     * @return 今天最大的时间
     */
    public static Date getTodayMax() {
        return Date.from(LocalDateTime.of(LocalDateTime.now().toLocalDate(), LocalTime.MAX).atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 格式化日期
     *
     * @param date 日期
     * @return String类型的日期
     */
    public static String format(Date date) {
        return format(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 按照指定类型格式化日期
     *
     * @param date   日期
     * @param format 指定格式
     * @return String类型的日期
     */
    public static String format(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }

}
