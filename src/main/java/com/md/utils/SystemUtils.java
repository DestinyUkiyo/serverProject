package com.md.utils;

import com.md.entity.Api;
import com.md.entity.system.CommonResult;
import com.md.entity.system.Dictionaries;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 系统的工具类
 *
 * @author 刘骄阳
 * 2021-10-11 18:49
 */
public class SystemUtils {

    /**
     * 验证所有apiId是否是真实的
     *
     * @param requestMap redis获取的
     * @param apiList    需要判断的apiId集合
     * @return true, false
     */
    public static Boolean allApiIsTrue(HashMap<Long, Api> requestMap, ArrayList<String> apiList) {
        //判断权限id是否都是真实的
        for (String id : apiList) {
            //id是否是null,id是否是非数字,id是否真实存在
            if (StringUtils.isBlank(id) || !id.matches(Dictionaries.IS_NUMBER) ||
                    requestMap.get(Long.parseLong(id)) == null) {
                return false;
            }
        }
        return true;
    }
}
