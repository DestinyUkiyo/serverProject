package com.md.utils;

import java.util.*;

/**
 * @author 刘骄阳
 * 2021-10-03 18:32
 */
public class CollectionUtils extends org.springframework.util.CollectionUtils {

    /**
     * 获取col1与col2的差集
     *
     * @param collection1 集合1
     * @param collection2 想要求差集的集合
     * @return 两个集合的差集
     */
    public static List getDifferenceSet(List collection1, List collection2) {
        HashSet<Object> set = new HashSet<>();
        set.addAll(collection1);
        set.addAll(collection2);
        set.removeAll(collection2);
        return new ArrayList<>(set);
    }
}
