package com.md.utils;

import com.md.entity.Users;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author 刘骄阳
 */
@Component
public class RequestContextHolderUtil {


    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    public static Users getUserInfo() {
        try {
            return (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取用户id
     *
     * @return 用户id
     */
    public static Long getUserId() {
        //登录之后，才能这里获取到userId，如果登录失效，或者没登录，无法获取。返回null
        Users userInfo = getUserInfo();
        if (userInfo != null) {
            return userInfo.getUId();
        } else {
            return null;
        }
    }
}
