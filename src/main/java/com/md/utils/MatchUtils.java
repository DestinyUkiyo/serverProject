package com.md.utils;

import com.md.entity.system.Dictionaries;

/**
 * 校验工具类
 *
 * @author 刘骄阳
 * 2021-09-17 23:26
 */
public class MatchUtils {
    /**
     * 校验是否是数字
     *
     * @param number 要判断的字符
     * @return 判断结果
     */
    public static Boolean isNumber(String number) {
        return number.matches(Dictionaries.IS_NUMBER);
    }
}
