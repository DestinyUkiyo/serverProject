package com.md.utils;

import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayRequest;
import com.alipay.api.internal.util.RequestParametersHolder;
import com.alipay.api.request.AlipayTradePagePayRequest;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author 刘骄阳
 * 2021-11-01 15:58
 */
public class ClassUtils {

    /**
     * 调用空参的私有方法
     *
     * @param clazz      哪一个类的私有方法
     * @param methodName 私有方法名
     * @param temp       从哪一个类调用
     * @return 操作结果
     */
    public static Object invokedMethod(Class<?> clazz, String methodName, Object temp) {
        return invokedMethod(clazz, methodName, new LinkedHashMap<>(0), temp);
    }

    /**
     * 调用私有方法
     *
     * @param clazz      调用哪一个类的私有方法
     * @param methodName 调用的方法的名字
     * @param map        map,存储参数名,和类型
     * @param temp       从哪一个类里调用
     * @return 操作结果
     */
    public static Object invokedMethod(Class<?> clazz, String methodName, LinkedHashMap<Object, Class<?>> map, Object temp) {
        try {
            //从map中取出,键和值
            Class<?>[] propertyType = new Class<?>[map.size()];
            Object[] propertyValues = new Object[map.size()];
            int i = 0;
            for (Object key : map.keySet()) {
                propertyType[i] = map.get(key);
                propertyValues[i] = key;
                i++;
            }
            //设置调用的方法,与参数类型
            Method method = clazz.getDeclaredMethod(methodName, propertyType);
            //设置可访问
            method.setAccessible(true);
            //调用,并传值
            return method.invoke(temp, propertyValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 调用私有方法
     *
     * @param clazz         调用哪一个类的私有方法
     * @param methodName    调用的方法的名字
     * @param propertyValue 参数值
     * @param propertyType  参数类型
     * @param temp          从哪一个类里调用
     * @return 操作结果
     */
    public static Object invokedMethod(Class<?> clazz, String methodName, ArrayList<Object> propertyValue, ArrayList<Class<?>> propertyType, Object temp) {
        try {
            //从map中取出,键和值
            Class<?>[] propertyTypes = new Class<?>[propertyType.size()];
            Object[] propertyValues = new Object[propertyValue.size()];
            for (int i = 0; i < propertyType.size(); i++) {
                propertyTypes[i] = propertyType.get(i);
                propertyValues[i] = propertyValue.get(i);
            }
            //设置调用的方法,与参数类型
            Method method = clazz.getDeclaredMethod(methodName, propertyTypes);
            //设置可访问
            method.setAccessible(true);
            //调用,并传值
            return method.invoke(temp, propertyValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 调用私有方法
     *
     * @param clazz         调用哪一个类的私有方法
     * @param methodName    调用的方法的名字
     * @param propertyValue 参数值
     * @param temp          从哪一个类里调用
     * @return 操作结果
     */
    public static Object invokedMethod(Class<?> clazz, String methodName, ArrayList<?> propertyValue, Object temp) {
        try {
            //从map中取出,键和值
            Class<?>[] propertyTypes = new Class<?>[propertyValue.size()];
            Object[] propertyValues = new Object[propertyValue.size()];
            for (int i = 0; i < propertyValue.size(); i++) {
                propertyTypes[i] = propertyValue.get(i).getClass();
                propertyValues[i] = propertyValue.get(i);
            }
            //设置调用的方法,与参数类型
            Method method = clazz.getDeclaredMethod(methodName, propertyTypes);
            //设置可访问
            method.setAccessible(true);
            //调用,并传值
            return method.invoke(temp, propertyValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 获取私有属性的值
     *
     * @param propertyName 私有属性的名字
     * @param obj          从哪一个类获取值
     * @return 私有属性的值
     */
    public static Object getField(String propertyName, Object obj) {
        try {
            //通过类获取
            Field field = obj.getClass().getDeclaredField(propertyName);
            //设置可访问
            field.setAccessible(true);
            //参数为实例化对象
            return field.get(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 获取私有属性的值,指定类
     *
     * @param clazz        类
     * @param propertyName 私有属性的名字
     * @param obj          从哪一个类获取值
     * @return 私有属性的值
     */
    public static Object getField(Class<?> clazz, String propertyName, Object obj) {
        try {
            //通过类获取
            Field field = clazz.getDeclaredField(propertyName);
            //设置可访问
            field.setAccessible(true);
            //参数为实例化对象
            return field.get(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private String demo(AlipayTradePagePayRequest request, AlipayClient alipayClient) throws Exception {
        //用到反射自然需要通过反射来获取类的对象，
        //通过class..forName(类路径.类名)获取
        Class<?> clazz = Class.forName("com.alipay.api.AbstractAlipayClient");
        //需要获取一个类对象
        //通过clazz.getDeclaredMethod获取方法，参数是方法名称及方法需要传的参数
        Method getRequestHolderWithSign = clazz.getDeclaredMethod("getRequestHolderWithSign", AlipayRequest.class,
                String.class, String.class,
                String.class, String.class);
        getRequestHolderWithSign.setAccessible(true);
        //获取私有属性的方法
        //参数为属性名
        Field appCertSN = clazz.getDeclaredField("appCertSN");
        appCertSN.setAccessible(true);
        //获取属性值
        //参数为实例化对象
        String fieldValue = (String) appCertSN.get(alipayClient);

        Object requestParametersHolder = getRequestHolderWithSign.invoke(alipayClient, request, null, null, fieldValue, null);


        Method method = clazz.getDeclaredMethod("getRedirectUrl", RequestParametersHolder.class);


        //需要设置一下是否检查，false的话或报错
        method.setAccessible(true);
        //最后通过invoke 方法调用方法,获取的就是方法的返回值
        Object obj = method.invoke(alipayClient, (RequestParametersHolder) requestParametersHolder);
        System.err.println("-------------------------------------");
        System.err.println(obj);
        System.err.println("-------------------------------------");
        return obj.toString();
    }

}
