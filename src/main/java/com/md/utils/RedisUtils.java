package com.md.utils;

import com.alibaba.fastjson.JSON;
import com.google.gson.reflect.TypeToken;
import com.md.entity.Api;
import com.md.entity.Role;
import com.md.entity.Users;
import com.md.entity.system.Dictionaries;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author 86156
 */
@Component
public class RedisUtils {

    @Resource(name = "customRedisTemplate")
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 根据key删除缓存
     */
    public void remove(final String... keys) {
        for (String key : keys) {
            if (exists(key)) {
                redisTemplate.delete(key);
            }
        }
    }


    /**
     * 根据key判断缓存是否存在数据
     */
    private Boolean exists(final String key) {
        Boolean flag = redisTemplate.hasKey(key);
        if (flag == null) {
            flag = false;
        }
        return flag;
    }

    /**
     * 根据key获取value,没有返回null
     *
     * @param key key
     */
    public String get(final String key) {
        //先判断是否存在
        if (!exists(key)) {
            //不存在直接返回null
            return null;
        }
        ValueOperations<String, String> operations = redisTemplate.opsForValue();
        return operations.get(key);
    }


    /**
     * 写入缓存
     *
     * @param key   key
     * @param value value
     */
    public boolean set(final String key, Object value) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, Dictionaries.GSON.toJson(value));
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 写入缓存,设置有效期
     *
     * @param key        key
     * @param value      value
     * @param expireTime 有效时间(秒)
     */
    public boolean set(final String key, Object value, Long expireTime) {
        boolean result = false;
        try {
            redisTemplate.opsForValue().set(key, Dictionaries.GSON.toJson(value));
            redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取后端使用的api
     * key: apiId
     * value: api对象
     *
     * @return 后端使用的api
     */
    public HashMap<Long, Api> getRequestMap() {
        String temp = get("requestMap");
        if (temp != null) {
            return Dictionaries.GSON.fromJson(temp, new TypeToken<HashMap<Long, Api>>() {
            }.getType());
        }
        return new HashMap<>(0);
    }

    /**
     * 获取返回给页面的接口
     * key: 模块名
     * value: 该模块下的api集合
     *
     * @return 返回给页面的接口
     */
    public HashMap<String, ArrayList<Api>> getWebRequest() {
        String temp = get("webRequest");
        if (temp != null) {
            return Dictionaries.GSON.fromJson(temp, new TypeToken<HashMap<String, ArrayList<Api>>>() {
            }.getType());
        }
        return new HashMap<>(0);
    }

    /**
     * 获取所有角色与api的关联
     * key: roleId
     * value: apiId,apiId
     *
     * @return 角色与api的关联
     */
    public HashMap<Long, String> getRoleApiMap() {
        String temp = get("roleApiMap");
        if (temp != null) {
            return Dictionaries.GSON.fromJson(temp, new TypeToken<HashMap<Long, String>>() {
            }.getType());
        }
        return new HashMap<>(0);
    }

    /**
     * 获取需要权限的map集合
     * key: 请求路径
     * value: 需要的角色id
     *
     * @return 需要权限的map集合
     */
    public HashMap<String, ArrayList<ConfigAttribute>> getSecurityMap() {
        String temp = get("securityMap");
        if (temp != null) {
            return Dictionaries.GSON.fromJson(temp, new TypeToken<HashMap<String, ArrayList<SecurityConfig>>>() {
            }.getType());
        }
        return new HashMap<>(0);
    }

    /**
     * 获取角色集合
     *
     * @return 角色集合
     */
    public ArrayList<Role> getRoleList() {
        String temp = get("roleList");
        if (temp != null) {
            return Dictionaries.GSON.fromJson(temp, new TypeToken<ArrayList<Role>>() {
            }.getType());
        }
        return new ArrayList<>(0);
    }

    /**
     * 获取在线用户集合
     * <加密字符串,Users>
     * key: 当前时间戳(token)使用MD5Utils加密后的数据
     * value: 登录账号
     *
     * @return 角色集合
     */
    public HashMap<String, Users> getOnlyUser() {
        String temp = get("onlyUser");
        if (temp != null) {
            return Dictionaries.GSON.fromJson(temp, new TypeToken<HashMap<String, Users>>() {
            }.getType());
        }
        return new HashMap<>(0);
    }

}
