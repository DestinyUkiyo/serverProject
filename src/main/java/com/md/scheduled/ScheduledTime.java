package com.md.scheduled;

import com.md.utils.DateUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author 刘骄阳
 * 2021-11-04 14:32
 */
@Component
public class ScheduledTime {

    /**
     * 订单超时处理
     */
//    @Scheduled(cron = "0 */1 * * * ?")
    public void timeOutOperation() {
        System.out.println(DateUtils.format(new Date()));
    }
}
