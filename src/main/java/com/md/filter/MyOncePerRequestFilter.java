package com.md.filter;

import com.md.utils.RedisUtils;
import com.md.utils.RequestUtil;
import com.md.utils.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义的过滤器,设置只过滤,/api和/system
 * 配置过滤器
 * - @WebFilter 设置过滤器过滤范围
 *
 * @author 刘骄阳
 * 2021-09-17 11:23
 */
@WebFilter(urlPatterns = {"/api/*", "/system/*"})
public class MyOncePerRequestFilter extends OncePerRequestFilter {
    @Resource
    RedisUtils redisUtils;

    /**
     * 先走这里然后判断是否登录
     *
     * @param request     request
     * @param response    response
     * @param filterChain filterChain
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        System.out.println("自定义过滤器执行: " + request.getRequestURI());
        String ipAddress = RequestUtil.getIPAddress(request);
        if (StringUtils.isNotBlank(ipAddress)) {
            redisUtils.set(ipAddress, redisUtils.get(ipAddress) == null ? 0 : Integer.parseInt(redisUtils.get(ipAddress)) + 1);
            System.out.println(ipAddress + ": 请求了" + redisUtils.get(ipAddress) + "次");
        }
        filterChain.doFilter(request, response);
    }
}
