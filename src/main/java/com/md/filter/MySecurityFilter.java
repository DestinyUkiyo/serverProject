package com.md.filter;


import com.md.entity.Users;
import com.md.entity.system.Dictionaries;
import com.md.utils.RedisUtils;
import com.md.utils.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author 刘骄阳
 * 2021-10-07 14:54
 */
public class MySecurityFilter implements Filter {
    private RedisUtils redis;

    public MySecurityFilter(RedisUtils redis) {
        this.redis = redis;
    }

    public MySecurityFilter() {
    }

    /**
     * 自定义的过滤器,用于Security
     *
     * @param temp     request
     * @param response response
     * @param chain    chain
     */
    @Override
    public void doFilter(ServletRequest temp, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) temp;
        System.err.println("security过滤器执行: " + request.getRequestURI());
        String token = request.getHeader("token");
        //判断是否传递token
        if (StringUtils.isNotBlank(token)) {
            //验证是否为纯数字
            if (token.matches(Dictionaries.IS_NUMBER)) {
                //从字典中取出
                Users users = redis.getOnlyUser().get(token);
                //判断是否为null
                if (users != null) {
                    //说明全都符合,存进上下文中
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(users, null, users.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        chain.doFilter(temp, response);
    }
}
