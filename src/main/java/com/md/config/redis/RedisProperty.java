package com.md.config.redis;

import lombok.Data;

/**
 * @author 刘骄阳
 * 2021-10-09 10:05
 */
@Data
public class RedisProperty {
    /**
     * 连接主机
     */
    private String host;
    /**
     * 端口
     */
    private Integer port;
    /**
     * 密码
     */
    private String password;
    /**
     * 连接超时时间
     */
    private Integer timeout;
    /**
     * 最大连接数
     */
    private Integer maxTotal;
    /**
     * 最大空闲连接数
     */
    private Integer maxIdle;
    /**
     * 最小空闲连接数
     */
    private Integer minIdle;
    /**
     * 最大等待时间
     */
    private Integer maxWaitTime;
    /**
     * 从连接池获取对象时,是否进行检查
     */
    private Boolean testOnBorrow;
    /**
     * 归还连接时,是否进行检查
     */
    private Boolean testOnReturn;
    /**
     * 连接耗尽,是否进入堵塞
     */
    private Boolean isBlocked;
    /**
     * 连接库
     */
    private Integer dataBase;
}
