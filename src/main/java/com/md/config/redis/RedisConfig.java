package com.md.config.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.Resource;

/**
 * @author 86156
 */
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport {

    @Resource
    RedisProperty redisProperty;

    /**
     * 生产key的策略
     */
    @Bean
    @Override
    public KeyGenerator keyGenerator() {
        return (target, method, params) -> {
            StringBuilder sb = new StringBuilder();
            sb.append(target.getClass().getName());
            sb.append(method.getName());
            for (Object obj : params) {
                sb.append(obj.toString());
            }
            return sb.toString();
        };

    }

    /**
     * redis 数据库连接池
     */
    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration redisConfig = new RedisStandaloneConfiguration();
        //设置主机
        redisConfig.setHostName(redisProperty.getHost());
        //设置密码
        redisConfig.setPassword(redisProperty.getPassword());
        //设置端口
        redisConfig.setPort(redisProperty.getPort());
        //设置连接库
        redisConfig.setDatabase(redisProperty.getDataBase());
        //获取默认的连接池构造器
        JedisClientConfiguration.JedisPoolingClientConfigurationBuilder redisPoolingBuilder = (JedisClientConfiguration.JedisPoolingClientConfigurationBuilder) JedisClientConfiguration.builder();
        JedisPoolConfig config = new JedisPoolConfig();
        //设置最大连接数
        config.setMaxTotal(redisProperty.getMaxTotal());
        //设置最大空闲连接数
        config.setMaxIdle(redisProperty.getMaxIdle());
        //设置最小空闲连接数
        config.setMinIdle(redisProperty.getMinIdle());
        //设置 连接耗尽进入阻塞
        config.setBlockWhenExhausted(redisProperty.getIsBlocked());
        //阻塞等待时间
        config.setMaxWaitMillis(redisProperty.getMaxWaitTime());
        //从连接池获取对象时,是否进行检查
        config.setTestOnBorrow(redisProperty.getTestOnBorrow());
        //归还连接时,是否进行检查
        config.setTestOnReturn(redisProperty.getTestOnReturn());
        //修改默认的连接池构造器
        redisPoolingBuilder.poolConfig(config);
        return new JedisConnectionFactory(redisConfig, redisPoolingBuilder.build());
    }

    /**
     * redisTemplate配置
     */
    @Bean(name = "customRedisTemplate")
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {
        StringRedisTemplate template = new StringRedisTemplate(factory);
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.activateDefaultTyping(om.getPolymorphicTypeValidator(), ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }

    @Bean
    RedisMessageListenerContainer container() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory());
        return container;
    }


}
