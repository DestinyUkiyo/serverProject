package com.md.config.mvc.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 配置拦截器
 *
 * @author 刘骄阳
 * 2021-07-03 10:12
 */
@Slf4j
public class MyAsyncHandlerInterceptor implements AsyncHandlerInterceptor {
    static Long nowTime;

    /**
     * 在业务处理器处理请求之前调用
     * Controller处理请求前调用
     * 预处理，可以进行编码、安全控制、权限校验等处理
     *
     * @param request  request
     * @param response response
     * @param handler  响应的处理器，自定义Controller
     * @return true:继续执行,调用下一个拦截器或处理器<p>false:中断执行,不会继续调用其他的拦截器或处理器,需要通过response来产生响应;
     * @throws Exception Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.err.println("拦截器开始执行");
        nowTime = System.currentTimeMillis();
        return true;
    }

    /**
     * 在业务处理器处理异步请求前,开始执行
     * Controller处理异步请求前开始执行
     *
     * @param request
     * @param response
     * @param handler
     * @throws Exception
     */
    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    }

    /**
     * 在业务处理器处理请求执行完成后，生成视图之前执行。
     * Controller处理完请求开始执行
     * 后处理（调用了Service并返回ModelAndView，但未进行页面渲染），有机会修改ModelAndView
     *
     * @param request      request
     * @param response     response
     * @param handler      响应的处理器，自定义Controller
     * @param modelAndView modelAndView
     * @throws Exception Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //业务逻辑处理完成,将上下文中存储的数据清除
        SecurityContextHolder.getContext().setAuthentication(null);
        System.out.println("用时: " + ((System.currentTimeMillis() - nowTime)) + "毫秒");
        System.err.println("拦截器结束执行");
    }

    /**
     * 在DispatcherServlet完全处理完请求后被调用。
     * 可用于清理资源等。返回处理（已经渲染了页面）
     *
     * @param request  request
     * @param response response
     * @param handler  响应的处理器，自定义Controller
     * @param ex       ex
     * @throws Exception Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

}
