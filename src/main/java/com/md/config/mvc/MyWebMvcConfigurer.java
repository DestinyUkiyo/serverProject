package com.md.config.mvc;


import com.md.config.mvc.interceptor.MyAsyncHandlerInterceptor;
import com.md.entity.system.Dictionaries;
import com.md.entity.system.MyConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-07-15 18:16
 */
@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {
    @Resource
    MyConfig myConfig;

    /**
     * 指定当前操作系统
     */
    private static final String osName = "WIN";

    /**
     * 视图跳转控制器
     *
     * @param registry registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //registry.addViewController("/").setViewName("index");
    }

    /**
     * 配置内容裁决的一些选项
     *
     * @param configurer configurer
     */
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    }

    /**
     * 默认静态资源处理器
     *
     * @param configurer configurer
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//        configurer.enable();
    }

    /**
     * 拦截器配置
     * 需要一个实现HandlerInterceptor接口的拦截器实例
     *
     * @param registry registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //addPathPatterns: 设置拦截器的过滤路径规则
        //excludePathPatterns: 设置不需要拦截的过滤规则
        registry.addInterceptor(new MyAsyncHandlerInterceptor()).addPathPatterns("/**").excludePathPatterns("/static/**", myConfig.loadFileUrl);
    }

    /**
     * 静态资源处理
     *
     * @param registry registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //判断是否为指定的操作系统
        if (Dictionaries.OS_NAME.toUpperCase().startsWith(osName)) {
            //是Windows
            registry.addResourceHandler(myConfig.loadFileUrl).addResourceLocations(myConfig.windowsFileUrl);
        } else {
            //是Linux
            registry.addResourceHandler(myConfig.loadFileUrl).addResourceLocations(myConfig.linuxFileUrl);
        }
    }

    /**
     * 配置跨域问题
     * <p>
     * addMapping: 配置可以被跨域的路径;<p>
     * allowedOrigins: 允许设置的域名访问资源,可以固定单条或者多条内容,如: "http://www.baidu.com",只有百度可以访问我们的跨域资源。
     * allowCredentials: 是否允许请求带有验证信息,用户是否可以发送、处理 cookie
     * allowedMethods: 设置允许的请求方法类型访问该跨域资源服务器,如: POST,GET,PUT,OPTIONS,DELETE等。
     * allowedHeaders: 允许所有的请求header访问,可以自定义设置任意请求头信息,如: "X-YYYY-TOKEN"
     * maxAge: 配置客户端缓存预检请求的响应的时间（以秒为单位）。默认设置为1800秒（30分钟）
     *
     * @param registry registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("*")
                .allowedHeaders("*")
                .maxAge((3600L * 24) * 7);
    }

    /**
     * 配置视图解析器
     *
     * @param registry registry
     */
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {

    }
}
