package com.md.config.pay.ali;

import com.alipay.api.AlipayApiException;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import lombok.Data;

/**
 * @author 刘骄阳
 * 2021-11-01 19:28
 */
@Data
public class AliPayConfig {
    /**
     * appId
     */
    private String appId;

    /**
     * 私钥
     */
    private String privateKey;

    /**
     * 异步回调地址
     */
    private String notifyUrl;

    /**
     * 支付宝网关
     */
    private String serverUrl;
    /**
     * 支付宝网关(沙箱)
     */
    private String devServerUrl;

    /**
     * 返回格式
     */
    private String format;

    /**
     * 签名方式
     */
    private String signType;

    /**
     * 字符编码格式
     */
    private String charset;

    /**
     * 应用公钥证书
     */
    private String appCertPath;

    /**
     * 支付宝公钥证书
     */
    private String aliPayCertPath;

    /**
     * 支付宝根证书
     */
    private String aliPayRootCertPath;

    /**
     * 初始化证书的请求
     *
     * @return DefaultAlipayClient
     */
    public DefaultAlipayClient initCertClient() {
        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
        certAlipayRequest.setServerUrl(devServerUrl);
        certAlipayRequest.setAppId(appId);
        certAlipayRequest.setPrivateKey(privateKey);
        certAlipayRequest.setFormat(format);
        certAlipayRequest.setCharset(charset);
        certAlipayRequest.setSignType(signType);
        certAlipayRequest.setCertPath(appCertPath);
        certAlipayRequest.setAlipayPublicCertPath(aliPayCertPath);
        certAlipayRequest.setRootCertPath(aliPayRootCertPath);
        try {
            return new DefaultAlipayClient(certAlipayRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return null;
    }
}
