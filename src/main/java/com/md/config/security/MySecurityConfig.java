package com.md.config.security;

import com.md.filter.MySecurityFilter;
import com.md.handler.security.MyAccessDeniedHandler;
import com.md.handler.security.MyAuthenticationEntryPoint;
import com.md.filter.MyOncePerRequestFilter;
import com.md.service.RoleApiService;
import com.md.service.RoleService;
import com.md.service.impl.LoginServiceImpl;
import com.md.service.impl.MyUserDetailsServiceImpl;
import com.md.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

/**
 * @author 刘骄阳
 * 2021-09-17 10:28
 */
@Configuration
@RequiredArgsConstructor
public class MySecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * 登录
     */
    private final UserDetailsService userDetailsService;
    /**
     * 未登录,或token失效
     */
    private final MyAccessDeniedHandler handler;
    /**
     * 没有权限
     */
    private final MyAuthenticationEntryPoint point;

    private final MyFilterInvocationSecurityMetadataSource myFilterInvocationSecurityMetadataSource;
    private final RedisUtils redisUtils;

    /**
     * 自定义的Security权限控制器
     *
     * @return securityFilter
     */
    public MySecurityFilter securityFilter() {
        return new MySecurityFilter(redisUtils);
    }

    /**
     * 自定义密码验证
     *
     * @return 对象
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new MyPasswordEncoder();
    }

    /**
     * 配置认证管理器
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * 使用自定义的用户名和密码
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //通过实现类,自定义用户名和密码
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    /**
     * 配置权限
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //自定义权限决策器
        http.authorizeRequests().anyRequest().authenticated().withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
            /**
             * 指定security使用自定义的权限决策器,而不是默认的
             */
            @Override
            public <O extends FilterSecurityInterceptor> O postProcess(O fis) {
                //自定义的权限决策器
                fis.setAccessDecisionManager(new MyAccessDecisionManager());
                //自定义FilterInvocationSecurityMetadataSource
                myFilterInvocationSecurityMetadataSource.init();
                fis.setSecurityMetadataSource(myFilterInvocationSecurityMetadataSource);
                return fis;
            }
        });
        //关闭csrf防护
        http.csrf().disable();
        //禁用匿名用户
        http.anonymous().disable();
        //配置filter过滤器
        http.addFilterBefore(securityFilter(), UsernamePasswordAuthenticationFilter.class);
        //配置没有权限与没有登录返回值
        http.exceptionHandling().accessDeniedHandler(handler).authenticationEntryPoint(point);
    }


}
