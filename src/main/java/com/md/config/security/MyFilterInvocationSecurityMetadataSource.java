package com.md.config.security;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.md.utils.RedisUtils;
import com.md.dao.RoleApiMapper;
import com.md.entity.Api;
import com.md.entity.RoleApi;
import com.md.entity.system.Dictionaries;
import com.md.entity.system.MyConfig;
import com.md.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 刘骄阳
 * 2021-10-03 15:40
 */
@Component
@RequiredArgsConstructor
public class MyFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    /**
     * 角色与api关联
     */
    private final RoleApiMapper roleApiMapper;
    /**
     * 自定义的配置文件
     */
    private final MyConfig myConfig;
    private final RedisUtils redis;
    private final SecurityUtils securityUtils;

    /**
     * 初始化
     * 注:之所以不在构造方法里写,是因为,roleApiMapper还没有初始化
     */
    public void init() {
        System.err.println("--------------------------------------");
        System.err.println("\t\t\t自定义权限配置");
        System.err.println("--------------------------------------");
        //获取角色,与角色的权限,并初始化
        securityUtils.init(roleApiMapper.selectList(new QueryWrapper<>()));
    }


    /**
     * 获取访问路径所需要的权限
     *
     * @param object 路径
     * @return 所需权限
     */
    @Override
    public ArrayList<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        //todo 有个问题,将id拼接到路径上如何操作
        //todo 未来解决,目前先将带拼接的除去,转为传参
        HttpServletRequest request = ((FilterInvocation) object).getRequest();
        return securityUtils.authorityCheck(request.getRequestURI());
    }

    /**
     * 获取所有权限资源
     */
    @Override
    public ArrayList<ConfigAttribute> getAllConfigAttributes() {
        System.out.println("莫名2");
        return null;
    }

    /**
     * 类对象是否支持校验
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
    }

}
