package com.md.config.security;

import com.md.entity.Users;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 自定义权限决策管理器
 *
 * @author 刘骄阳
 * 2021-10-03 16:49
 */
@Component
public class MyAccessDecisionManager implements AccessDecisionManager {


    protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

    /**
     * 权限决策:
     * 如果不报异常,则允许通过,否则无权
     *
     * @param authentication   当前登录用户的角色信息
     * @param object           FilterInvocation对象，可以得到request等web资源
     * @param configAttributes 当前请求需要的角色(可能有多个)
     */
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        Users users = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (authentication.getPrincipal() == null || configAttributes.size() == 0) {
            //说明没有登录,或者该接口尚未配置权限,报出无法访问的接口
            throw new AccessDeniedException(messages.getMessage(
                    "AbstractAccessDecisionManager.accessDenied", "权限不足"));
        }
        ArrayList<ConfigAttribute> list = (ArrayList<ConfigAttribute>) configAttributes;
        //鉴权
        boolean flag = true;
        for (ConfigAttribute configAttribute : list) {
            if (configAttribute.getAttribute().equalsIgnoreCase(users.getRId().toString())) {
                //说明当前用户有调用该接口的权限
                flag = false;
                break;
            }
        }
        if (flag) {
            throw new InsufficientAuthenticationException(messages.getMessage(
                    "AbstractSecurityInterceptor.authenticationNotFound", "验证失败"));
        }
    }

    /**
     * Indicates whether this <code>AccessDecisionManager</code> is able to process
     * authorization requests presented with the passed <code>ConfigAttribute</code>.
     * <p>
     * This allows the <code>AbstractSecurityInterceptor</code> to check every
     * configuration attribute can be consumed by the configured
     * <code>AccessDecisionManager</code> and/or <code>RunAsManager</code> and/or
     * <code>AfterInvocationManager</code>.
     * </p>
     *
     * @param attribute a configuration attribute that has been configured against the
     *                  <code>AbstractSecurityInterceptor</code>
     * @return true if this <code>AccessDecisionManager</code> can support the passed
     * configuration attribute
     */
    @Override
    public boolean supports(ConfigAttribute attribute) {
        return false;
    }

    /**
     * Indicates whether the <code>AccessDecisionManager</code> implementation is able to
     * provide access control decisions for the indicated secured object type.
     *
     * @param clazz the class that is being queried
     * @return <code>true</code> if the implementation can process the indicated class
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }
}
