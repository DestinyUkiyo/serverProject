package com.md.config.security;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author 刘骄阳
 * 2021-09-17 10:30
 */
public class MyPasswordEncoder implements PasswordEncoder {
    /**
     * 加密操作
     *
     * @param rawPassword 原始密码
     */
    @Override
    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    /**
     * 匹配两个密码是否一致
     *
     * @param rawPassword     原始密码
     * @param encodedPassword 加密后的密码
     * @return 是否一致
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return rawPassword.equals(encodedPassword);
    }
}
