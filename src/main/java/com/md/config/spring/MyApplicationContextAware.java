package com.md.config.spring;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.md.config.pay.ali.AliPayConfig;
import com.md.utils.RedisUtils;
import com.md.dao.ApiMapper;
import com.md.dao.RoleApiMapper;
import com.md.dao.RoleMapper;
import com.md.entity.Api;
import com.md.entity.system.Dictionaries;
import com.md.utils.CollectionUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 * @author 刘骄阳
 * 2021-09-19 14:14
 */
@Component
@RequiredArgsConstructor
public class MyApplicationContextAware implements ApplicationContextAware {
    private final ApiMapper apiMapper;
    private final RoleApiMapper roleApiMapper;
    private final RoleMapper roleMapper;
    private final RedisUtils redis;
    private final AliPayConfig aliPayConfig;

    /**
     * 初始化applicationContext的时候调用
     * 每一次读取都是固定的,除非Controller中做了其他修改
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.err.println("--------------------------------------");
        System.err.println("\t\t\t初始化缓存数据");
        System.err.println("--------------------------------------");
        Long time = System.currentTimeMillis();

        //获取使用RequestMapping注解标注的类
        Map<String, Object> beans = applicationContext.getBeansWithAnnotation(RestController.class);
        ArrayList<Api> apiListTemp = new ArrayList<>();
        HashMap<String, ArrayList<Api>> webRequest = new HashMap<>(16);

        beans.forEach((key, value) -> {
            Class<?> clazz = AopUtils.getTargetClass(value);
            //获取这个类上的RequestMapping注解
            RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
            ArrayList<Api> list = new ArrayList<>();
            Arrays.asList(clazz.getDeclaredMethods()).forEach(temp -> {
                //循环判断当前方法是什么类型
                for (int i = 0; i < Dictionaries.REQUEST_CLASS.size(); i++) {
                    Annotation annotation = temp.getAnnotation(Dictionaries.REQUEST_CLASS.get(i));
                    //如果不等于null,说明类型匹配上了
                    if (annotation != null) {
                        //生成一个请求方法对象
                        Api api = Api.init(annotation.toString(), requestMapping.value()[0]);
                        list.add(api);
                        break;
                    }
                }
            });
            webRequest.put(requestMapping.name(), list);
            apiListTemp.addAll(list);
        });

        //将api存入数据库,先从数据库中取出所有数据,然后逐个比对是否存在,如果不存在,存入数据库
        List<Api> apiList = apiMapper.selectList(new QueryWrapper<>());
        //获取两个集合的差集
        List<Api> differenceList = CollectionUtils.getDifferenceSet(apiListTemp, apiList);
        //将不存在数据库的api添加进数据库
        differenceList.forEach(apiMapper::insert);
        //key: apiId value: api对象;
        HashMap<Long, Api> map = new HashMap<>(16);
        //判断从数据库中获取的数据是否等于生成
        //将不存在的数据存进字典中
        differenceList.addAll(apiList);
        differenceList.forEach(api -> map.put(api.getApiId(), api));
        //将返回给页面的数据进行处理
        webRequest.forEach((key, apis) -> apis.forEach(api -> {
            for (Long apiId : map.keySet()) {
                if (map.get(apiId).equals(api)) {
                    //说明是一个对象
                    api.setApiId(apiId);
                    api.setApiUrl(null);
                    break;
                }
            }
        }));
        //将角色数据存入redis
        redis.set("roleList", roleMapper.selectList(new QueryWrapper<>()));
        //将后端使用的api存入redis
        redis.set("requestMap", map);
        //将返回给页面的api存入redis
        redis.set("webRequest", webRequest);

        System.out.println("初始化完成: " + (System.currentTimeMillis() - time) + "毫秒");

    }
}
