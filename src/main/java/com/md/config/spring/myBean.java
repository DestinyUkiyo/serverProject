package com.md.config.spring;

import com.md.config.pay.ali.AliPayConfig;
import com.md.config.redis.RedisProperty;
import com.md.entity.system.MyConfig;
import com.md.filter.MySecurityFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 刘骄阳
 * 2021-10-03 14:21
 */
@Configuration
public class myBean {

    /**
     * 自定义的配置,上传文件路径,访问地址
     *
     * @return myConfig
     */
    @Bean
    @ConfigurationProperties(prefix = "custom")
    public MyConfig myConfig() {
        return new MyConfig();
    }

    /**
     * 自定义的配置,redis的属性配置
     *
     * @return redisProperty
     */
    @Bean
    @ConfigurationProperties(prefix = "custom.redis")
    public RedisProperty redisProperty() {
        return new RedisProperty();
    }

    @Bean
    @ConfigurationProperties(prefix = "custom.pay.ali")
    public AliPayConfig aliPayConfig() {
        return new AliPayConfig();
    }
}
