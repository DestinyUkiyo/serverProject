package com.md.config.swagger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 配置Swagger
 *
 * @author 刘骄阳
 * 2021-09-18 22:31
 */
@Configuration
@EnableSwagger2
public class MySwaggerConfig {
    @Value("${custom.swagger.enable}")
    Boolean enable;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).enable(enable).
                select().apis(RequestHandlerSelectors.basePackage("com.md.controller")).
                paths(PathSelectors.any()).build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("个人练习网站")
                .description("ljy")
                .contact(new Contact("刘骄阳", "http://baidu.com", "2985966466@qq.com"))
                .version("1.0.0").build();
    }
}
