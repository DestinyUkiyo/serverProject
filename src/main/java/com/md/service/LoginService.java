package com.md.service;

import com.md.entity.Users;
import com.md.entity.system.CommonResult;

/**
 * @author 刘骄阳
 * 2021-09-17 10:54
 */
public interface LoginService {
    /**
     * 通过账号登录
     *
     * @param users users对象
     * @return 操作结果
     */
    CommonResult login(Users users);
}
