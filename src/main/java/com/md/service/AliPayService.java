package com.md.service;

import com.md.entity.system.CommonResult;

/**
 * @author 刘骄阳
 * 2021-10-27 17:43
 */
public interface AliPayService {
    /**
     * 支付宝手机网页支付
     *
     * @return 操作结果
     */
    CommonResult mobileHtmlPay();
}
