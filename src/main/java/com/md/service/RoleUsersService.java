package com.md.service;

import com.md.entity.RoleUsers;
import com.md.entity.system.CommonResult;
import com.md.entity.system.Page;

/**
 * @author 刘骄阳
 * 2021-09-17 18:38
 */
public interface RoleUsersService {
    /**
     * 关联用户与角色
     *
     * @param roleUsers 对象
     * @return 操作结果
     */
    CommonResult insert(RoleUsers roleUsers);

    /**
     * 修改用户与角色
     *
     * @param roleUsers 对象
     * @return 操作结果
     */
    CommonResult update(RoleUsers roleUsers);

    /**
     * 用户与角色关联列表
     *
     * @param page 分页
     * @return 用户与角色关联列表
     */
    CommonResult select(Page page);

    /**
     * 删除用户与角色关联关系
     *
     * @param id id
     * @return 操作结果
     */
    CommonResult delete(Long id);
}
