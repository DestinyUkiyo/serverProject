package com.md.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.md.dao.RoleUsersMapper;
import com.md.dao.UsersMapper;
import com.md.entity.Role;
import com.md.entity.RoleUsers;
import com.md.entity.Users;
import com.md.entity.system.CommonResult;
import com.md.entity.system.Page;
import com.md.service.RoleUsersService;
import com.md.utils.RedisUtils;
import com.md.utils.RequestContextHolderUtil;
import com.md.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 刘骄阳
 * 2021-09-17 18:38
 */
@Service
@RequiredArgsConstructor
public class RoleUsersServiceImpl implements RoleUsersService {
    private final RoleUsersMapper roleUsersMapper;
    private final UsersMapper usersMapper;
    private final RedisUtils redis;

    /**
     * 关联用户与角色
     *
     * @param roleUsers 对象
     * @return 操作结果
     */
    @Override
    public CommonResult insert(RoleUsers roleUsers) {
        Users users = usersMapper.selectById(roleUsers.getUId());
        if (users == null) {
            return CommonResult.error("检测到非法请求");
        }
        if (roleUsersMapper.insert(roleUsers) > 0) {
            //添加成功后,将用户踢出登录
            redis.set("onlyUser", users.removeMap(redis.getOnlyUser()));
            return CommonResult.success("操作成功!");
        }
        return CommonResult.error("操作失败!");
    }

    /**
     * 修改用户与角色
     *
     * @param roleUsers 对象
     * @return 操作结果
     */
    @Override
    public CommonResult update(RoleUsers roleUsers) {
        //有参数传递过来
        if (roleUsers != null) {
            //角色id和用户id不等于null
            if (roleUsers.getRId() != null && roleUsers.getUId() != null) {
                //校验用户id是否存在
                if (StringUtils.isNotBlank(redis.getRoleApiMap().get(roleUsers.getRId()))) {
                    UpdateWrapper<RoleUsers> wrapper = new UpdateWrapper<>();
                    //根据用户id修改
                    wrapper.eq("u_id", roleUsers.getUId());
                    wrapper.set("r_id", roleUsers.getRId());
                    roleUsersMapper.update(new RoleUsers(), wrapper);
                    return CommonResult.success("修改成功!");
                }
            }
        }
        return CommonResult.error("检测到非法请求!");
    }

    /**
     * 用户与角色关联列表
     *
     * @param page 分页
     * @return 用户与角色关联列表
     */
    @Override
    public CommonResult select(Page page) {
        IPage<Users> iPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(page.getPageNum(), page.getPageSize());
        //分页查询
        IPage<Users> list = usersMapper.selectPage(iPage, new QueryWrapper<Users>() {
            {
                this.select("u_enable", "u_name", "u_head_icon", "u_frozen", "u_id", "u_account", "u_create_date");
            }
        });
        List<Users> records = list.getRecords();
        //获取用户id
        List<Long> userIds = records.stream().map(Users::getUId).collect(Collectors.toList());
        QueryWrapper<RoleUsers> wrapper = new QueryWrapper<>();
        wrapper.in("u_id", userIds);
        //查询跟获取到的用户相关的数据
        List<RoleUsers> roleUsers = roleUsersMapper.selectList(wrapper);
        //将用户id和角色id存入map
        Map<Long, Long> rIds = roleUsers.stream().collect(Collectors.toMap(RoleUsers::getUId, RoleUsers::getRId));
        //将角色id和角色名存入map
        Map<Long, String> rNames = redis.getRoleList().stream().collect(Collectors.toMap(Role::getRId, Role::getRName));

        records.forEach(users -> {
            //根据用户id查询角色id
            users.setRId(rIds.get(users.getUId()));
            //根据角色id查询角色名
            users.setRName(rNames.get(users.getRId()));
        });
        return CommonResult.success(list);
    }

    /**
     * 删除用户与角色关联关系
     *
     * @param id id
     * @return 操作结果
     */
    @Override
    public CommonResult delete(Long id) {
        if (id == null) {
            return CommonResult.error("检测到非法请求!");
        }
        //验证用户
        if (id.equals(RequestContextHolderUtil.getUserId())) {
            return CommonResult.error("不能对自己进行操作");
        }
        Users users = usersMapper.selectById(id);
        if (users == null) {
            return CommonResult.error("用户不存在");
        }
        if (roleUsersMapper.deleteById(id) > 0) {
            //删除成功的时候要将,用户踢出登录
            redis.set("onlyUser", users.removeMap(redis.getOnlyUser()));
            return CommonResult.success("删除成功!");
        }
        return CommonResult.error("删除失败");
    }
}
