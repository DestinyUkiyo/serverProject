package com.md.service.impl;

import com.md.dao.ApiMapper;
import com.md.entity.Api;
import com.md.entity.system.CommonResult;
import com.md.service.ApiService;
import com.md.utils.RedisUtils;
import com.md.utils.SecurityUtils;
import com.md.utils.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author 刘骄阳
 * 2021-09-17 19:06
 */
@Service
@RequiredArgsConstructor
public class ApiServiceImpl implements ApiService {
    private final ApiMapper apiMapper;
    private final RedisUtils redis;

    /**
     * 添加api接口
     *
     * @param api 对象
     * @return 操作结果
     */
    @Override
    public CommonResult insert(Api api) {
        if (apiMapper.insert(api) > 0) {
            return CommonResult.success("添加成功!");
        }
        return CommonResult.error("添加失败!");
    }

    /**
     * 获取api列表
     *
     * @return api列表
     */
    @Override
    public CommonResult select() {
        return CommonResult.success(redis.getWebRequest());
    }

    /**
     * 修改api接口
     * 修改功能名
     *
     * @param api 对象
     * @return 操作结果
     */
    @Override
    public CommonResult update(Api api) {
        if (api.getApiId() != null && StringUtils.isNotBlank(api.getApiName())) {
            api.setApiUrl(null);
            if (apiMapper.updateById(api) > 0) {
                //修改成功之后,将返回给页面的数据整理
                boolean flag = false;
                HashMap<String, ArrayList<Api>> webRequest = redis.getWebRequest();
                for (String key : webRequest.keySet()) {
                    for (Api temp : webRequest.get(key)) {
                        if (temp.getApiId().equals(api.getApiId())) {
                            //说明找到指定api对象了
                            flag = true;
                            temp.setApiName(api.getApiName());
                        }
                    }
                    if (flag) {
                        break;
                    }
                }
                redis.set("webRequest", webRequest);
                return CommonResult.success("修改成功!");
            }
        }
        return CommonResult.error("修改失败!");
    }

    /**
     * 删除api接口
     * 不允许操作
     *
     * @param id id
     * @return 操作结果
     */
    @Override
    public CommonResult delete(Long id) {
        if (id != null && apiMapper.deleteById(id) > 0) {
            return CommonResult.success("删除成功!");
        }
        return CommonResult.error("删除失败!");
    }
}
