package com.md.service.impl;

import com.md.utils.RedisUtils;
import com.md.entity.system.CommonResult;
import com.md.service.SystemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author 刘骄阳
 * 2021-09-19 14:00
 */
@Service
@RequiredArgsConstructor
public class SystemServiceImpl implements SystemService {
    private final RedisUtils redis;

    /**
     * 获取全部api接口
     *
     * @return api接口列表
     */
    @Override
    public CommonResult getAllRequest() {
        return CommonResult.success(redis.getRequestMap());
    }

}
