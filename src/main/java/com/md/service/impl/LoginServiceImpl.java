package com.md.service.impl;

import com.md.dao.UsersMapper;
import com.md.entity.Users;
import com.md.entity.system.CommonResult;
import com.md.service.LoginService;
import com.md.utils.MD5Utils;
import com.md.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @author 刘骄阳
 * 2021-09-17 10:54
 */
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {
    private final AuthenticationManager authenticationManager;
    private final UsersMapper usersMapper;
    private final RedisUtils redis;

    /**
     * 通过账号登录
     *
     * @param users users对象
     * @return 操作结果
     */
    @Override
    public CommonResult login(Users users) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(users.getUAccount().toString(), users.getUPassword()));
        Users user = (Users) authentication.getPrincipal();
        if (user == null) {
            return CommonResult.error("登录失败!");
        }
        //todo 校验用户是否被冻结或者锁住
        //设置最后登录时间
        user.setULastLoginTime(new Date(System.currentTimeMillis()));
        usersMapper.updateById(user);
        //判断用户是否登录过
        Map<String, Users> onlineUser = user.removeMap(redis.getOnlyUser());
        //生成token
        user.setToken(MD5Utils.MD5EncodeUtf8(System.currentTimeMillis()));
        //将对象存入集合中
        onlineUser.put(user.getToken(), user);
        redis.set("onlyUser", onlineUser);

        return CommonResult.success(user);
    }
}
