package com.md.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.md.dao.RoleUsersMapper;
import com.md.dao.UsersMapper;
import com.md.entity.Api;
import com.md.entity.RoleUsers;
import com.md.entity.Users;
import com.md.entity.system.Dictionaries;
import com.md.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 刘骄阳
 * 2021-10-08 0:44
 */
@Service
@RequiredArgsConstructor
public class MyUserDetailsServiceImpl implements UserDetailsService {
    private final UsersMapper usersMapper;
    private final RoleUsersMapper roleUsersMapper;
    private final RedisUtils redis;

    /**
     * 自定义登录功能
     */
    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        //可以在添加添加用户名的时候加上固定了两个字段,查出来的时候去掉,存进去的时候加上
        if (!account.matches(Dictionaries.IS_NUMBER)) {
            throw new UsernameNotFoundException("此账号不存在");
        }
        wrapper.eq("u_account", Long.parseLong(account));
        //获取用户
        Users users = usersMapper.selectOne(wrapper);
        //如果不存在,抛出异常
        if (users == null) {
            throw new UsernameNotFoundException("此账号不存在");
        }
        QueryWrapper<RoleUsers> roleUsersWrapper = new QueryWrapper<>();
        roleUsersWrapper.eq("u_id", users.getUId());
        //根据用户id查询所属角色id,不可能为null,创建用户的时候,会有默认角色
        RoleUsers roleUsers = roleUsersMapper.selectOne(roleUsersWrapper);
        //赋值角色id
        users.setRId(roleUsers.getRId());
        //根据角色id获取权限
        List<String> list = Arrays.asList(redis.getRoleApiMap().get(users.getRId()).split(","));
        ArrayList<Api> authorities = new ArrayList<>();
        //循环赋值
        list.forEach(aId -> authorities.add(new Api(Long.parseLong(aId))));
        //存入集合
        users.setAuthorities(authorities);
        return users;
    }

}
