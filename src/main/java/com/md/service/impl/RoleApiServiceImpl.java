package com.md.service.impl;

import com.md.entity.Api;
import com.md.utils.RedisUtils;
import com.md.dao.RoleApiMapper;
import com.md.entity.RoleApi;
import com.md.entity.system.CommonResult;
import com.md.entity.system.Dictionaries;
import com.md.service.RoleApiService;
import com.md.utils.SecurityUtils;
import com.md.utils.StringUtils;
import com.md.utils.SystemUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @author 刘骄阳
 * 2021-09-17 23:03
 */
@Service
@RequiredArgsConstructor
public class RoleApiServiceImpl implements RoleApiService {
    private final RoleApiMapper roleApiMapper;
    private final RedisUtils redis;
    private final SecurityUtils securityUtils;

    /**
     * 关联角色和api
     *
     * @param roleApi 对象
     * @return 操作结果
     */
    @Override
    public CommonResult insert(RoleApi roleApi) {
        if (roleApiMapper.insert(roleApi) > 0) {
            return CommonResult.success("关联成功!");
        }
        return CommonResult.error("关联失败!");
    }

    /**
     * 关联角色和api
     *
     * @param roleApi 对象
     * @return 操作结果
     */
    @Override
    public CommonResult update(RoleApi roleApi) {
        //判断值是否都传递了过来,并且角色id是真实的
        if (roleApi != null && roleApi.getRId() != null && StringUtils.isNotBlank(redis.getRoleApiMap().get(roleApi.getRId()))) {
            ArrayList<String> apiList;
            //判断权限id,是否大于1
            if (StringUtils.isNotBlank(roleApi.getApiId()) && (apiList = new ArrayList<>(Arrays.asList(roleApi.getApiId().split(Dictionaries.SPLIT)))).size() > 0) {
                //判断权限id是否都是真实的
                HashMap<Long, Api> requestMap = redis.getRequestMap();
                if (!SystemUtils.allApiIsTrue(requestMap, apiList)) {
                    return CommonResult.error("检测到非法请求!");
                }
                //判断是否是同一个
                RoleApi temp = roleApiMapper.selectById(roleApi.getRaId());
                if (!temp.equals(roleApi)) {
                    return CommonResult.error("检测到非法请求!");
                }
                if (roleApiMapper.updateById(roleApi) > 0) {
                    //关联成功后,将字典中的数据重新处理
                    securityUtils.reloadRoleApi(roleApi.getRId(), apiList);
                    return CommonResult.success("关联成功!");
                }
            }
            return CommonResult.error("关联失败,角色最少要包含一个权限!");
        }
        return CommonResult.error("关联失败,未知角色!");
    }

    /**
     * 通过id查询角色拥有的权限
     *
     * @param id 角色id
     * @return 操作结果
     */
    @Override
    public CommonResult select(Long id) {
        //可以不用查询数据库,直接获取
        String apiIds = redis.getRoleApiMap().get(id);
        if (StringUtils.isNotBlank(apiIds)) {
            return CommonResult.success(Arrays.asList(apiIds.split(Dictionaries.SPLIT)));
        }
        return CommonResult.error("检测到非法请求");
    }

    /**
     * 删除角色与api的关联关系
     * 不允许操作
     *
     * @param id 要删除的id
     * @return 操作结果
     */
    @Override
    public CommonResult delete(Long id) {
        if (roleApiMapper.deleteById(id) > 0) {
            return CommonResult.success("删除成功!");
        }
        return CommonResult.error("删除失败!");
    }
}
