package com.md.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.md.dao.RoleUsersMapper;
import com.md.entity.Api;
import com.md.entity.RoleUsers;
import com.md.utils.RedisUtils;
import com.md.dao.RoleApiMapper;
import com.md.dao.RoleMapper;
import com.md.entity.Role;
import com.md.entity.RoleApi;
import com.md.entity.system.CommonResult;
import com.md.entity.system.Dictionaries;
import com.md.service.RoleService;
import com.md.utils.SecurityUtils;
import com.md.utils.StringUtils;
import com.md.utils.SystemUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @author 刘骄阳
 * 2021-09-17 16:18
 */
@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {


    private final RoleMapper roleMapper;
    private final RoleApiMapper roleApiMapper;
    private final RoleUsersMapper roleUsersMapper;
    private final RedisUtils redis;
    private final SecurityUtils securityUtils;

    /**
     * 添加角色
     *
     * @param role 角色对象
     * @return 操作状态
     */
    @Override
    public CommonResult insert(Role role) {
        //角色名,角色拥有的权限
        if (role != null && StringUtils.isNotBlank(role.getRName())) {
            ArrayList<String> apiList;
            if (StringUtils.isNotBlank(role.getApiIds()) && (apiList = new ArrayList<>(Arrays.asList(role.getApiIds().split(Dictionaries.SPLIT)))).size() > 0) {
                HashMap<Long, Api> requestMap = redis.getRequestMap();
                //验证是否真实存在
                if (!SystemUtils.allApiIsTrue(requestMap, apiList)) {
                    return CommonResult.error("检测到非法请求!");
                }
                if (roleMapper.insert(role) > 0) {
                    //添加角色成功的时候,在角色与api表中添加对应数据
                    RoleApi roleApi = new RoleApi();
                    roleApi.setRId(role.getRId());
                    //权限赋值
                    roleApi.setApiId(role.getApiIds());
                    //添加进数据库
                    roleApiMapper.insert(roleApi);
                    //重新添加进redis
                    HashMap<Long, String> roleApiMap = redis.getRoleApiMap();
                    roleApiMap.put(roleApi.getRId(), roleApi.getApiId());
                    redis.set("roleApiMap", roleApiMap);
                    //实时添加进Security权限里
                    securityUtils.reloadRoleApi(role.getRId(), apiList);
                    return CommonResult.success("添加成功!");
                }
                return CommonResult.error("添加失败!");
            }
            return CommonResult.error("添加失败,角色最少要包含一个权限!");
        }
        return CommonResult.error("检测到非法请求!");
    }

    /**
     * 修改角色
     * 修改角色名
     *
     * @param role 角色对象
     * @return 操作状态
     */
    @Override
    public CommonResult update(Role role) {
        if (role != null && role.getRId() != null && StringUtils.isNotBlank(role.getRName())) {
            if (roleMapper.updateById(role) > 0) {
                //修改成功后,应同步更新redis中的数据
                ArrayList<Role> list = redis.getRoleList();
                list.forEach(temp -> {
                    if (temp.getRId().equals(role.getRId())) {
                        temp.setRName(role.getRName());
                    }
                });
                redis.set("roleList", list);
                return CommonResult.success("修改成功!");
            }
        }
        return CommonResult.error("修改失败!");
    }

    /**
     * 查询角色
     *
     * @return 角色列表
     */
    @Override
    public CommonResult select() {
        return CommonResult.success(redis.getRoleList());
    }

    /**
     * 删除角色
     *
     * @param id 要删除的角色id
     * @return 操作状态
     */
    @Override
    public CommonResult delete(Long id) {
        if (id != null) {
            //查询角色下是否有用户
            QueryWrapper<RoleUsers> wrapper = new QueryWrapper<>();
            wrapper.eq("r_id", id);
            //角色与用户的表也应该水平分表
            Integer count = roleUsersMapper.selectCount(wrapper);
            if (count > 0) {
                return CommonResult.error("该角色下含有用户,禁止删除");
            }
            //否则说明,角色下没有用户,可以删除
            roleMapper.deleteById(id);
            //删除后将,数据更新
            ArrayList<Role> roleList = redis.getRoleList();
            for (int i = 0; i < roleList.size(); i++) {
                //如果id相等
                if (roleList.get(i).getRId().equals(id)) {
                    roleList.remove(i);
                    break;
                }
            }
            //更新redis中的数据
            redis.set("roleList", roleList);
            //删除角色数据
            HashMap<Long, String> roleApiMap = redis.getRoleApiMap();
            roleApiMap.remove(id);
            redis.set("roleApiMap", roleApiMap);
            HashMap<String, ArrayList<ConfigAttribute>> securityMap = redis.getSecurityMap();
            ArrayList<ConfigAttribute> list;
            for (String key : securityMap.keySet()) {
                //根据键获取值
                list = securityMap.get(key);
                for (int i = 0; i < list.size(); i++) {
                    //判断是否为要删除的数据
                    if (list.get(i).getAttribute().equalsIgnoreCase(id.toString())) {
                        //删除
                        list.remove(i);
                        //重新赋值
                        securityMap.put(key, list);
                        break;
                    }
                }
            }
            redis.set("securityMap", securityMap);
            return CommonResult.success("删除成功!");
        }
        return CommonResult.error("检测到非法请求!");
    }
}
