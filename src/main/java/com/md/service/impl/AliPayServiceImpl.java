package com.md.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.*;
import com.alipay.api.internal.util.RequestParametersHolder;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.md.config.pay.ali.AliPayConfig;
import com.md.entity.system.CommonResult;
import com.md.service.AliPayService;
import com.md.utils.ClassUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.UUID;

/**
 * @author 刘骄阳
 * 2021-10-27 17:43
 */
@Service
@RequiredArgsConstructor
public class AliPayServiceImpl implements AliPayService {
    private final AliPayConfig aliPayConfig;


    /**
     * 支付宝手机网页支付
     *
     * @return 操作结果
     */
    @Override
    public CommonResult mobileHtmlPay() {
        DefaultAlipayClient aliPayClient = aliPayConfig.initCertClient();
        if (aliPayClient != null) {
            AlipayTradeWapPayRequest request = new AlipayTradeWapPayRequest();
            request.setNotifyUrl(aliPayConfig.getNotifyUrl());
            request.setReturnUrl("");
            request.setBizContent(initOrder());
            Object appCertSN = ClassUtils.getField(AbstractAlipayClient.class, "appCertSN", aliPayClient);
            RequestParametersHolder requestParametersHolder =
                    (RequestParametersHolder) ClassUtils.invokedMethod(AbstractAlipayClient.class, "getRequestHolderWithSign",
                            new ArrayList<>(Arrays.asList(request, null, null, appCertSN, null)), new ArrayList<Class<?>>(Arrays.asList(AlipayRequest.class, String.class, String.class, String.class, String.class)) {
                            }, aliPayClient);
            Object obj = ClassUtils.invokedMethod(AbstractAlipayClient.class, "getRedirectUrl", new LinkedHashMap<Object, Class<?>>(1) {
                {
                    this.put(requestParametersHolder, RequestParametersHolder.class);
                }
            }, aliPayClient);
            System.err.println(obj);
        }
        return null;
    }

    /**
     * 初始化订单
     *
     * @return 订单参数
     */
    private String initOrder() {
        JSONObject bizContent = new JSONObject();
        //订单号
        bizContent.put("out_trade_no", UUID.randomUUID().toString());
        //订单金额,最低0.01
        bizContent.put("total_amount", 100);
        //订单标题
        bizContent.put("subject", "大保健预购");
        //销售产品码，商家和支付宝签约的产品码
        bizContent.put("product_code", "QUICK_WAP_WAY");
        //用户付款中途退出返回商户网站的地址
        bizContent.put("quit_url", "https://www.baidu.com");
        //绝对超时时间
        bizContent.put("time_expire", "2021-11-04 21:30:00");
        JSONArray goodsDetail = new JSONArray();
        goodsDetail.add(initGoodsDetail());
        //订单包含的商品列表信息
        bizContent.put("goods_detail", goodsDetail);
        return bizContent.toString();
    }

    /**
     * 初始化商品信息
     *
     * @return 商品信息
     */
    private JSONObject initGoodsDetail() {
        JSONObject goods = new JSONObject();
        //商品的编号
        goods.put("goods_id", "1");
        //商品的名称
        goods.put("goods_name", "大保健");
        //商品数量
        goods.put("quantity", 1);
        //商品单价
        goods.put("price", 100);
        //商品描述信息
        goods.put("body", "山东大保健欢迎您");
        return goods;
    }
}
