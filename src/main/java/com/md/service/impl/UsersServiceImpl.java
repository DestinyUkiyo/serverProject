package com.md.service.impl;

import com.md.dao.RoleUsersMapper;
import com.md.dao.UsersMapper;
import com.md.entity.RoleUsers;
import com.md.entity.Users;
import com.md.entity.system.CommonResult;
import com.md.service.UsersService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author 刘骄阳
 * 2021-09-16 22:19
 */
@Service
public class UsersServiceImpl implements UsersService {
    @Resource
    UsersMapper usersMapper;
    @Resource
    RoleUsersMapper roleUsersMapper;

    /**
     * 创建一个用户
     *
     * @param users 用户对象
     * @return 操作结果
     */
    @Override
    public CommonResult insert(Users users) {
        //生成账号
        users.setUAccount(0L);
        //设置默认的头像
        users.setUHeadIcon("");
        //设置创建时间
        users.setUCreateDate(new Date());
        //设置修改时间
        users.setUModifyDate(new Date());
        if (usersMapper.insert(users) > 0) {
            //创建完用户,还要赋予角色
            RoleUsers roleUsers = new RoleUsers();
            //新增的用户赋予普通用户权限
            roleUsers.setRId(1446412666787700737L);
            roleUsers.setUId(users.getUId());
            roleUsersMapper.insert(roleUsers);
            return CommonResult.success("创建成功!" + users.getUId());
        }
        return CommonResult.error("创建失败!");
    }

    /**
     * 注册用户
     *
     * @param users 用户对象
     * @return 操作结果
     */
    @Override
    public CommonResult registerUsers(Users users) {
        //生成账号
        users.setUAccount(0L);
        //设置默认的头像
        users.setUHeadIcon("");
        //设置创建时间
        users.setUCreateDate(new Date());
        //设置修改时间
        users.setUModifyDate(new Date());
        if (usersMapper.insert(users) > 0) {
            //创建完用户,还要赋予角色
            RoleUsers roleUsers = new RoleUsers();
            //新增的用户赋予普通用户权限
            roleUsers.setRId(1446412666787700737L);
            roleUsers.setUId(users.getUId());
            roleUsersMapper.insert(roleUsers);
            return CommonResult.success("创建成功!" + users.getUId());
        }
        return CommonResult.error("创建失败!");
    }
}
