package com.md.service;

import com.md.entity.system.CommonResult;

/**
 * @author 刘骄阳
 * 2021-09-19 13:59
 */
public interface SystemService {
    /**
     * 获取全部api接口
     *
     * @return api接口列表
     */
    CommonResult getAllRequest();
}
