package com.md.service;

import com.md.entity.Role;
import com.md.entity.system.CommonResult;

/**
 * @author 刘骄阳
 * 2021-09-17 16:18
 */
public interface RoleService {
    /**
     * 添加角色
     *
     * @param role 角色对象
     * @return 操作状态
     */
    CommonResult insert(Role role);

    /**
     * 修改角色
     * 修改角色名
     *
     * @param role 角色对象
     * @return 操作状态
     */
    CommonResult update(Role role);

    /**
     * 查询角色
     *
     * @return 角色列表
     */
    CommonResult select();

    /**
     * 删除角色
     *
     * @param id 要删除的角色id
     * @return 操作状态
     */
    CommonResult delete(Long id);
}
