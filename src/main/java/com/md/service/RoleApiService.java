package com.md.service;

import com.md.entity.RoleApi;
import com.md.entity.system.CommonResult;

/**
 * @author 刘骄阳
 * 2021-09-17 23:02
 */
public interface RoleApiService {
    /**
     * 关联角色和api
     *
     * @param roleApi 对象
     * @return 操作结果
     */
    CommonResult insert(RoleApi roleApi);

    /**
     * 关联角色和api
     *
     * @param roleApi 对象
     * @return 操作结果
     */
    CommonResult update(RoleApi roleApi);

    /**
     * 通过id查询角色拥有的权限
     *
     * @param id 角色id
     * @return 操作结果
     */
    CommonResult select(Long id);

    /**
     * 删除角色与api的关联关系
     * 不允许操作
     *
     * @param id 要删除的id
     * @return 操作结果
     */
    CommonResult delete(Long id);
}
