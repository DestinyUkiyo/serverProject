package com.md.service;

import com.md.entity.Api;
import com.md.entity.system.CommonResult;

/**
 * @author 刘骄阳
 * 2021-09-17 19:05
 */
public interface ApiService {
    /**
     * 添加api接口
     * 不允许操作
     *
     * @param api 对象
     * @return 操作结果
     */
    CommonResult insert(Api api);

    /**
     * 获取api列表
     *
     * @return api列表
     */
    CommonResult select();

    /**
     * 修改api接口
     * 修改功能名
     *
     * @param api 对象
     * @return 操作结果
     */
    CommonResult update(Api api);

    /**
     * 删除api接口
     * 不允许操作
     *
     * @param id id
     * @return 操作结果
     */
    CommonResult delete(Long id);
}
