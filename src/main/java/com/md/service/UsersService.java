package com.md.service;

import com.md.entity.Users;
import com.md.entity.system.CommonResult;

/**
 * @author 刘骄阳
 * 2021-09-16 22:18
 */
public interface UsersService {
    /**
     * 创建一个用户
     *
     * @param users 用户对象
     * @return 操作结果
     */
    CommonResult insert(Users users);

    /**
     * 注册用户
     *
     * @param users 用户对象
     * @return 操作结果
     */
    CommonResult registerUsers(Users users);
}
